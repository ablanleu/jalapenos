# What is it

It is one of numerous attempts to write an OS using Rust programming language.
This one is based on an OS used for education: [NachOS]().

# Compile and Run

You need the Rust RISC-V toolchain, python3, and qemu.
Depending on your distribution, qemu executables may have different names. In
order to run the project, make sure the right executable name is used for the
`runner` variable in the [cargo config file](.cargo/config).

## Install Rust toolchain

You need the `nightly-riscv32imac-unknown-none-elf` toolchain. If you have
rustup, you can install it with this command line:
```bash
$ rustup toolchain install nightly-riscv32imac-unknown-none-elf
```

## Why python ?

When building, a custom build script is used to build the assembly code of the
bootloader. This script also runs some python to parse a specific file
([src/trap.rs](src/trap.rs)) in order to synchronize the user system library (in
[userlib](userlib)) with the trap handler.

## Run !

Once everything is installed, you can run it like any other Rust program using
`cargo run` (or `cargo run --release`). To stop the program, as the runner is
qemu, you need to use the qemu exit key combination <kbd>Ctrl-A</kbd> <kbd>X</kbd>.

# Debug ?

If you have RISC-V binutils, you can use GDB. Make sure to add -s and -S options
to the runner arguments (in the [cargo config file](.cargo/config)).

When running with these options, qemu will wait for a GDB instance to connect
before doing anything. You can now run GDB, and `source` one of the 2 script files
to connect. If you ran the simulator with `cargo run`, use `gdb_debug`. If you
ran with `cargo run --release`, use `gdb_release`.
