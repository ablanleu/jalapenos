import regex as re

header = """#ifndef __SYS_H__
#define __SYS_H__
"""

footer = "#endif // __SYS_H__\n"

state = 0

codename = re.compile(r"\w+")
proto = re.compile(r"//\s*([\w\*]+)\s*\(([^,].*)(,[^,]*)*\)")

codenames = []
prototypes = []

with open("src/trap.rs", "r") as src:
    # get all syscall codenames
    # and prototypes
    for src_line in src.readlines():
        if state == 0:
            if src_line.startswith("enum SyscallCode"):
                state = 1
        elif state == 1:
            if src_line.startswith("}"):
                state = 2
            else:
                m = codename.search(src_line)
                name = m.group(0)
                codenames.append(name)

                m = proto.search(src_line)
                prototypes.append(m.groups()[:-1])

# generate sys.h file, which consists of a list of syscall prototypes
with open("userlib/sys.h", "w") as dst:
    # header
    dst.write(header)

    # write prototypes
    for proto, name in zip(prototypes, codenames):
        dst.write("{} {}({});\n".format(proto[0], name, ", ".join(proto[1:])))


    # footer
    dst.write(footer)

# Generate sys.S file which implements syscall functions in asm.
# We implement them in asm because these functions do not follow classic
# C call convention.
with open("userlib/sys.S", "w") as dst:
    for i, ((ret, args), name) in enumerate(zip(prototypes, codenames)):
        dst.write(".global {}\n".format(name))
        dst.write("{}:\n".format(name))
        dst.write("\tli a7, {}\n".format(i))
        dst.write("\tecall\n")
        dst.write("\tret\n")
