extern crate cc;

use std::error::Error;
use cc::Build;
use std::process::Command;
use std::fs::File;

fn main() -> Result<(), Box<dyn Error>> {

    let out = File::create("build_libc.out")?;
    let err = File::create("build_libc.err")?;

    let status = Command::new("python")
        .arg("build_c_user_lib.py")
        .stdout(out)
        .stderr(err)
        .status()?;

    assert!(status.success(), "Could not generate userlib files, see build_libc.out and build_libc.err for more information");

    Build::new()
        .file("src/asm/boot.S")
        .file("src/asm/trap.S")
        .file("src/asm/constants.S")
        .flag("-fPIC")
        .flag("-Tlayout.lds")
        .flag("-ffreestanding")
        .flag("-nostdlib")
        .flag("-fno-rtti")
        .flag("-fno-exceptions")
        .flag("-mabi=ilp32")
        .flag("-march=rv32imac")
        .compile("asm");

    Ok(())
}
