
use core::{
    sync::atomic::{Ordering, AtomicUsize},
    cell::{Cell, UnsafeCell},
    mem::MaybeUninit,
};

pub struct LazySyncCell<T, F = fn() -> T> {
    state: AtomicUsize,
    inner: UnsafeCell<MaybeUninit<T>>,
    init: Cell<Option<F>>,
}

impl<T, F> LazySyncCell<T, F>
where F: FnOnce() -> T,
{
    pub const fn new(f: F) -> Self
    {
        Self {
            state: AtomicUsize::new(0),
            inner: UnsafeCell::new(MaybeUninit::uninit()),
            init: Cell::new(Some(f)),
        }
    }
}

impl<T> core::ops::Deref for LazySyncCell<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        match self.state.fetch_or(1, Ordering::AcqRel) {
            0 => { // uninit, we need to init it

                unsafe { self.inner.get().as_mut().unwrap().write(self.init.take().unwrap()()); }

                self.state.store(3, Ordering::Release);
            },
            3 => { // nothing to be done, just continue
            },
            _ => {
                // any other value means if is being inited, we wait for it
                loop {
                    if self.state.load(Ordering::Acquire) == 3 {
                        break
                    }
                }
            },
        }

        // SAFETY: self is safe because we only arrive here once state is 3 (initialized)
        unsafe { self.inner.get().as_ref().unwrap().assume_init_ref() }
    }
}

// SAFETY: LazySyncCell can take care of its own state (UNINIT, INITING, INIT) but it cannot take
// care of the state of its inner data. So it can only be Send or Sync if T is Send or Sync
unsafe impl<T : Send> Send for LazySyncCell<T> {} 
unsafe impl<T : Sync + Send> Sync for LazySyncCell<T> {} 
