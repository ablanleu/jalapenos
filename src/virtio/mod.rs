use core::num::NonZeroUsize;
use alloc::boxed::Box;
use crate::{
    platform::{VIRTIO, VIRTIO_STRIDE, VIRTIO_NUM},
    lock::{Mutex, MutexLock},
};

pub mod queue;
pub mod block;

#[repr(transparent)]
pub struct DeviceDescriptor {
    base: NonZeroUsize,
}

impl DeviceDescriptor {
    #[inline]
    const fn new(n:usize) -> Option<Self> {
        match NonZeroUsize::new(n) {
            None => None,
            Some(base) => Some(Self { base }),
        }
    }

    #[inline]
    pub unsafe fn write_u128(&self, offset:MmioRegisterOffset, n:u128) {
        ((usize::from(self.base) + offset as usize) as *mut u128).write_volatile(n)
    }

    #[inline]
    pub unsafe fn read_u128(&self, offset:MmioRegisterOffset) -> u128 {
        ((usize::from(self.base) + offset as usize) as *mut u128).read_volatile()
    }

    #[inline]
    pub unsafe fn write_u64(&self, offset:MmioRegisterOffset, n:u64) {
        ((usize::from(self.base) + offset as usize) as *mut u64).write_volatile(n)
    }

    #[inline]
    pub unsafe fn read_u64(&self, offset:MmioRegisterOffset) -> u64 {
        ((usize::from(self.base) + offset as usize) as *mut u64).read_volatile()
    }

    #[inline]
    pub unsafe fn write_u32(&self, offset:MmioRegisterOffset, n:u32) {
        ((usize::from(self.base) + offset as usize) as *mut u32).write_volatile(n)
    }

    #[inline]
    pub unsafe fn read_u32(&self, offset:MmioRegisterOffset) -> u32 {
        ((usize::from(self.base) + offset as usize) as *mut u32).read_volatile()
    }

    #[inline]
    pub unsafe fn write_u16(&self, offset:MmioRegisterOffset, n:u16) {
        ((usize::from(self.base) + offset as usize) as *mut u16).write_volatile(n)
    }

    #[inline]
    pub unsafe fn read_u16(&self, offset:MmioRegisterOffset) -> u16 {
        ((usize::from(self.base) + offset as usize) as *mut u16).read_volatile()
    }

    #[inline]
    pub unsafe fn write_u8(&self, offset:MmioRegisterOffset, n:u8) {
        ((usize::from(self.base) + offset as usize) as *mut u8).write_volatile(n)
    }

    #[inline]
    pub unsafe fn read_u8(&self, offset:MmioRegisterOffset) -> u8 {
        ((usize::from(self.base) + offset as usize) as *mut u8).read_volatile()
    }

    #[inline]
    pub unsafe fn read_config<T>(&self, offset:usize) -> T {
        ((usize::from(self.base) + MmioRegisterOffset::Config as usize + offset) as *mut T).read_volatile()
    }

    pub unsafe fn setup_virtqueue<const S: usize>(&self, id: u32, flags: u16) -> Box<queue::VirtQueue<S>> {
        let queue = queue::VirtQueue::new(flags);
        
        self.write_u32(MmioRegisterOffset::QueueSel, id);

        crate::cpu::fence_global();

        self.write_u32(MmioRegisterOffset::QueueNum, S as u32);

        if (self.read_u32(MmioRegisterOffset::QueueNumMax) as usize) < S {
            println!("Queue size asked is too high");
        }

        // because we have a 1:1 mapping of kernel virtual memory to physical memory, we don't have
        // to translate addresses

        self.write_u32(MmioRegisterOffset::QueueDescLow, queue.desc() as *const _ as u32);
        self.write_u32(MmioRegisterOffset::QueueDescHigh, 0);

        self.write_u32(MmioRegisterOffset::QueueDriverLow, queue.avail() as *const _ as u32);
        self.write_u32(MmioRegisterOffset::QueueDriverHigh, 0);

        self.write_u32(MmioRegisterOffset::QueueDeviceLow, queue.used() as *const _ as u32);
        self.write_u32(MmioRegisterOffset::QueueDeviceHigh, 0);

        crate::cpu::fence_global();

        self.write_u32(MmioRegisterOffset::QueueReady, 1);

        queue
    }
}

#[repr(usize)]
pub enum MmioRegisterOffset {
    Magic             = 0x000,
    Version           = 0x004,
    DeviceID          = 0x008,
    VendorID          = 0x00c,
    DeviceFeatures    = 0x010,
    DeviceFeaturesSel = 0x014,
    DriverFeatures    = 0x020,
    DriverFeaturesSel = 0x024,
    DriverPageSize    = 0x028, // Legacy register
    QueueSel          = 0x030,
    QueueNumMax       = 0x034,
    QueueNum          = 0x038,
    QueueAlign        = 0x03c, // Legacy driver
    QueuePFN          = 0x040, // Legacy driver
    QueueReady        = 0x044,
    QueueNotify       = 0x050,
    InterruptStatus   = 0x060,
    InterruptACK      = 0x064,
    Status            = 0x070,
    QueueDescLow      = 0x080,
    QueueDescHigh     = 0x084,
    QueueDriverLow    = 0x090,
    QueueDriverHigh   = 0x094,
    QueueDeviceLow    = 0x0a0,
    QueueDeviceHigh   = 0x0a4,
    ConfigGeneration  = 0x0fc,
    Config            = 0x100,
}

const MAGIC : u32 = 0x74726976;

pub fn discover_devices() {
    for i in 0..VIRTIO_NUM {
        let addr = unsafe { VIRTIO.add(i * VIRTIO_STRIDE) as *mut u32 };
        let magic = unsafe { addr.read_volatile() };
        if magic == MAGIC {
            let devversion = unsafe { addr.add(1).read_volatile() };
            let devid = unsafe { addr.add(2).read_volatile() };

            match devid {
                0 => continue,
                2 => {
                    print!("Trying to initialize block device version {} at address {:x}... ", devversion, addr as usize);
                    
                    let desc = DeviceDescriptor::new(addr as usize).unwrap();
                    let ok = block::initialize(desc);
                    if ok {
                        println!("OK");
                    } else {
                        println!("ERR");
                    }
                },
                _ => {
                    println!("Unknown VirtIO device (Device ID {}) at address {:x}", devid, addr as usize);
                },
            }
        }
    }
}

pub fn run_command<const S: usize, const N: usize>(descriptor: DeviceDescriptor, queue: &mut queue::VirtQueue<S>, layout: [(u64, u32, u16); N]) {
    let _ = queue.alloc_desc(layout);
    unsafe { descriptor.write_u32(MmioRegisterOffset::QueueNotify, 0) }
}
