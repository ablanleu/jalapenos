use super::{
    MmioRegisterOffset,
    DeviceDescriptor,
    queue::VirtQueue,
};
use crate::{
    lock::Mutex,
    scheduler::Thread,
};
use core::ptr::NonNull;
use alloc::boxed::Box;

#[derive(Debug)]
#[repr(C)]
struct BlockGeometry {
    cylinders: u16,
    heads: u8,
    sectors: u8,
}

#[derive(Debug)]
#[repr(C)]
struct BlockTopology {
    physical_block_exp: u8,
    alignment_offset: u8,
    min_io_size: u16,
    opt_io_size: u32,
}

#[derive(Debug)]
#[repr(C)]
struct BlockDeviceConfig {
    capacity: u64,
    size_max: u32,
    seg_max: u32,
    geometry: BlockGeometry,
    blk_size: u32,
    topology: BlockTopology,
    writeback: u8,
    padding1: [u8; 3],
    max_discard_sectors: u32,
    max_discard_seg: u32,
    discard_sector_alignment: u32,
    max_write_zeroes_sectors: u32,
    max_write_zeroes_seg: u32,
    write_zeroes_may_unmap: u8,
    padding2: [u8; 3],
}

struct BlkDevice {
    desc: DeviceDescriptor,
    queue: Box<VirtQueue<128>>,
    pending_query: Option<UserQuery>,
}
// SAFETY: this is safe because the only pointer we use (pending_query) is sure to point to a
// sleeping thread and thuss we only access it while it does not run
unsafe impl Send for BlkDevice {}

struct UserQuery {
    thread: Box<Thread>,
    virtual_address: NonNull<u8>,
    sectors_remaining: usize,
    query: Query,
}

#[repr(C)]
struct Query {
    typ: u32,
    reserved: u32,
    sector: u64,
    status: u8,
}

static DEVICE : Mutex<Option<Box<BlkDevice>>> = Mutex::new(None);

pub(super) fn initialize(desc: DeviceDescriptor) -> bool {

    let mut dev = DEVICE.spin_lock();
    if dev.is_some() { return false }

    use MmioRegisterOffset as Off;
    unsafe {

        // RESET
        desc.write_u32(Off::Status, 0);

        // ACKNOWLEDGE
        desc.write_u32(Off::Status, 0b1);

        // DRIVER
        desc.write_u32(Off::Status, 0b11);

        // Set Driver flags
        desc.write_u32(Off::DeviceFeaturesSel, 0);
        let device_features = desc.read_u32(Off::DeviceFeatures) & !(1 << 29);
        desc.write_u32(Off::DriverFeaturesSel, 0);
        desc.write_u32(Off::DriverFeatures, device_features);

        // Set page size
        //desc.write_u32(Off::DriverPageSize, 4096);

        // FEATURES_OK
        desc.write_u32(Off::Status, 0b1011);

        // ensure FEATURES_OK was accepted
        let status = desc.read_u32(Off::Status);
        if (status & 0b1000) == 0 {
            return false;
        }

        let queue = desc.setup_virtqueue(0, 0);

        // DRIVER_OK
        desc.write_u32(Off::Status, 0b1111);

        *dev = Some(Box::new(BlkDevice {
            desc,
            queue,
            pending_query: None,
        }));
    }

    true
}

pub fn read_syscall(thread: Box<Thread>, sector: usize, to_read: &mut [u8]) -> Option<()> {
    let len = to_read.len();
    if len % 512 != 0 { return None }

    let ptr = to_read.as_ptr();

    DEVICE
        .spin_lock()
        .as_mut()
        .and_then(|dev| {

            if dev.pending_query.is_some() { return None }

            dev.pending_query = Some(UserQuery {
                thread,
                virtual_address: NonNull::new(ptr as *mut u8)?,
                sectors_remaining: len / 512,
                query: Query {
                    typ: 0,
                    reserved: 0,
                    sector: sector as u64,
                    status: 0,
                },
            });

            let proc = dev.pending_query.as_ref().unwrap().thread.process.spin_lock();
            let phys = proc
                .address_space()
                .virt_to_phys(ptr as usize)
                .and_then(|ptr| NonNull::new(ptr as *mut u8))?;

            let info = &dev.pending_query.as_ref().unwrap().query;
            let info_typ_phys = proc
                .address_space()
                .virt_to_phys(&(info.typ) as *const _ as usize)?;
            let info_status_phys = proc
                .address_space()
                .virt_to_phys(&(info.typ) as *const _ as usize)?;

            // send memory zones descriptor chain in the queue
            dev.queue.alloc_desc([
                (info_typ_phys as u64, 16, 0),     // first 16 bytes of the query
                (phys.as_ptr() as u64, 512, 2),      // data buffer
                (info_status_phys as u64, 1, 2), // last byte of the query (status byte)
            ]);

            // notify the device we have prepared a query
            unsafe { dev.desc.write_u32(MmioRegisterOffset::QueueNotify, 0); }

            Some(())
        })
}

pub fn write_syscall(thread: Box<Thread>, offset: usize, to_write: &mut [u8]) -> Option<()> {
    let len = to_write.len();
    if len % 512 != 0 { return None }
    let ptr = to_write.as_ptr();

    DEVICE
        .spin_lock()
        .as_mut()
        .and_then(|dev| {

            if dev.pending_query.is_some() { return None }

            dev.pending_query = Some(UserQuery {
                thread,
                virtual_address: NonNull::new(ptr as *mut u8)?,
                sectors_remaining: len / 512,
                query: Query {
                    typ: 1,
                    reserved: 0,
                    sector: (offset / 512) as u64,
                    status: 0,
                },
            });

            let proc = dev.pending_query.as_ref().unwrap().thread.process.spin_lock();
            let phys = proc
                .address_space()
                .virt_to_phys(ptr as usize)
                .and_then(|ptr| NonNull::new(ptr as *mut u8))?;

            let info = &dev.pending_query.as_ref().unwrap().query;
            let info_typ_phys = proc
                .address_space()
                .virt_to_phys(&(info.typ) as *const _ as usize)?;
            let info_status_phys = proc
                .address_space()
                .virt_to_phys(&(info.typ) as *const _ as usize)?;

            // send memory zones descriptor chain in the queue
            dev.queue.alloc_desc([
                (info_typ_phys as u64, 16, 0),     // first 16 bytes of the query
                (phys.as_ptr() as u64, 512, 2),      // data buffer
                (info_status_phys as u64, 1, 2), // last byte of the query (status byte)
            ]);

            // notify the device we have prepared a query
            unsafe { dev.desc.write_u32(MmioRegisterOffset::QueueNotify, 0); }

            Some(())
        })
}

pub fn handle_request() -> Option<()> {
    DEVICE
        .spin_lock()
        .as_mut()
        .and_then(|mut dev| {
            let mut q = dev.pending_query.take()?;

            // acknowledge the finished query
            let status = unsafe { dev.desc.read_u32(MmioRegisterOffset::InterruptStatus) };
            unsafe { dev.desc.write_u32(MmioRegisterOffset::InterruptACK, status); }

            dev.queue.clear_events();

            q.virtual_address = NonNull::new(unsafe { q.virtual_address.as_ptr().add(512) })?;
            q.sectors_remaining = q.sectors_remaining.saturating_sub(1);

            if q.sectors_remaining == 0 {
                let thread = q.thread;
                crate::scheduler::Thread::wakeup(thread)
            } else {
                q.query.status = 0;
                q.query.sector += 1;

                dev.pending_query = Some(q);
                let q = dev.pending_query.as_ref().unwrap();

                let proc = q.thread.process.spin_lock();
                let phys = proc
                    .address_space()
                    .virt_to_phys(q.virtual_address.as_ptr() as usize)
                    .and_then(|ptr| NonNull::new(ptr as *mut u8))?;

                let info = &q.query;
                let info_typ_phys = proc
                    .address_space()
                    .virt_to_phys(&(info.typ) as *const _ as usize)?;
                let info_status_phys = proc
                    .address_space()
                    .virt_to_phys(&(info.typ) as *const _ as usize)?;

                // send memory zones descriptor chain in the queue
                dev.queue.alloc_desc([
                    (info_typ_phys as u64, 16, 0),     // first 16 bytes of the query
                    (phys.as_ptr() as u64, 512, 2),      // data buffer
                    (info_status_phys as u64, 1, 2), // last byte of the query (status byte)
                ]);

                // notify the device we have prepared a query
                unsafe { dev.desc.write_u32(MmioRegisterOffset::QueueNotify, 0); }
            }
            Some(())
        })
}

/// Read a sector of the block device and actively wait for query completion.
/// This function is to be used by the kernel before interrupts are enabled in order to
/// load some data from the block device (for example the first process to run).
///
/// We need this kind of function because we don't want to pollute our kernel stack before
/// switching to user mode.
fn _read_blocking(dev: &mut BlkDevice, sector: u32, to_read: &mut [u8; 512]) -> Option<()> {

    let data = to_read;

    let info = Query {
        typ: 0,
        reserved: 0,
        sector: sector as u64,
        status: 0,
    };

    // send memory zones descriptor chain in the queue
    dev.queue.alloc_desc([
        (&(info.typ) as *const _ as u64, 16, 0),     // first 16 bytes of the query
        (data.as_ptr() as u64, data.len() as u32, 2),      // data buffer
        (&(info.status) as *const _ as u64, 1, 2), // last byte of the query (status byte)
    ]);

    // notify the device we have prepared a query
    unsafe { dev.desc.write_u32(MmioRegisterOffset::QueueNotify, 0); }

    // actively wait for the device to finish its query
    let status = loop {
        let status = unsafe { dev.desc.read_u32(MmioRegisterOffset::InterruptStatus) };
        if status != 0 { break status }
    };

    dev.queue.clear_events();

    // acknowledge the finished query
    unsafe { dev.desc.write_u32(MmioRegisterOffset::InterruptACK, status); }

    if info.status == 0 {
        Some(())
    } else {
        None
    }
}

/// Write a sector in the block device and actively wait for query completion.
/// This function is to be used by the kernel before interrupts are enabled in order to
/// write some data from the block device.
fn _write_blocking(dev: &mut BlkDevice, sector: u32, to_write: &[u8; 512]) -> Option<()> {

    let mut data = to_write;

    let info = Query {
        typ: 1,
        reserved: 0,
        sector: sector as u64,
        status: 0,
    };

    // send memory zones descriptor chain in the queue
    dev.queue.alloc_desc([
        (&(info.typ) as *const _ as u64, 16, 0),     // first 16 bytes of the query
        (to_write.as_ptr() as u64, to_write.len() as u32, 0),      // data buffer
        (&(info.status) as *const _ as u64, 1, 2), // last byte of the query (status byte)
    ]);

    // notify the device we have prepared a query
    unsafe { dev.desc.write_u32(MmioRegisterOffset::QueueNotify, 0); }

    // actively wait for the device to finish its query
    let status = loop {
        let status = unsafe { dev.desc.read_u32(MmioRegisterOffset::InterruptStatus) };
        if status != 0 { break status }
    };

    // cleanup used descriptors
    dev.queue.clear_events();

    // acknowledge the finished query
    unsafe { dev.desc.write_u32(MmioRegisterOffset::InterruptACK, status); }

    if info.status == 0 {
        Some(())
    } else {
        None
    }
}

pub fn read_blocking(sector: u32, to_read: &mut [u8; 512]) -> Option<()> {
    DEVICE
        .spin_lock()
        .as_mut()
        .and_then(|dev| {
            _read_blocking(dev, sector, to_read)
        })
}

pub fn write_blocking(sector: u32, to_write: &[u8; 512]) -> Option<()> {
    DEVICE
        .spin_lock()
        .as_mut()
        .and_then(|dev| {
            _write_blocking(dev, sector, to_write)
        })
}
