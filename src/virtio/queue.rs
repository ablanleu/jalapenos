use alloc::boxed::Box;

#[repr(C)]
#[derive(Clone, Copy)]
pub struct VirtQueueDesc {
    addr: u64,
    len: u32,
    flags: u16,
    next: VirtQueueDescId,
}

impl VirtQueueDesc {
    pub const fn new() -> Self {
        Self {
            addr: 0,
            len: 0,
            flags: 0,
            next: VirtQueueDescId { id: 0 },
        }
    }
}

impl VirtQueueDesc {
    const FLAG_NEXT : u16 = 1;
}

#[repr(transparent)]
#[derive(Clone, Copy, PartialEq)]
pub struct VirtQueueDescId {
    id: u16,
}

impl From<VirtQueueDescId> for usize {
    fn from(id: VirtQueueDescId) -> Self {
        id.id as usize
    }
}

#[repr(C, align(4))]
pub struct VirtQueueAvail<const S: usize> {
    flags: u16,
    idx: u16,
    ring: [VirtQueueDescId; S],
}

impl<const S: usize> VirtQueueAvail<S> {
    pub fn new(flags: u16) -> Self {
        Self {
            flags,
            idx: 0,
            ring: [VirtQueueDescId { id: 0 }; S],
        }
    }
}

#[repr(C, packed)]
#[derive(Clone, Copy)]
pub struct VirtQueueUsedElem {
    id: u32,
    len: u32,
}

#[repr(C)]
pub struct VirtQueueUsed<const S: usize> {
    flags: u16,
    idx: u16,
    ring: [VirtQueueUsedElem; S],
}

impl<const S: usize> VirtQueueUsed<S> {
    pub fn new(flags: u16) -> Self {
        Self {
            flags,
            idx: 0,
            ring: [VirtQueueUsedElem { id: 0, len: 0 }; S],
        }
    }
}

#[repr(C)]
pub struct VirtQueue<const S: usize> {
    /// bookeeping data
    first_free_desc: VirtQueueDescId,
    first_visible_used: usize,

    /// The virtqueue
    desc: [VirtQueueDesc; S],
    avail: VirtQueueAvail<S>,
    used: VirtQueueUsed<S>,

    chose: u64,
}

impl<const S: usize> VirtQueue<S> {
    pub fn new(flags: u16) -> Box<Self> {

        let mut this = Box::new(Self {
            first_free_desc: VirtQueueDescId { id: 0 },
            first_visible_used: 0,
            desc: [VirtQueueDesc::new(); S],
            avail: VirtQueueAvail::new(flags),
            used: VirtQueueUsed::new(flags),
            chose: 0,
        });

        for i in 0..S {
            this.desc[i].next = VirtQueueDescId { id: i as u16 + 1 };
        }

        this
    }

    pub fn alloc_desc<const N: usize>(&mut self, layout: [(u64, u32, u16); N]) -> [VirtQueueDescId; N] {
        let mut ret = [VirtQueueDescId { id: 0 }; N];

        // for each descriptor to create
        for (i, layout) in layout.iter().enumerate() {
            // allocate a descriptor
            let id = self.get_free_desc_id();
            let desc = self.descriptor_mut(id);

            // setup its information
            desc.addr = layout.0;
            desc.len = layout.1;
            desc.flags = layout.2;

            // save descriptor's id
            ret[i] = id;
        }

        for ids in ret.windows(2) {
            let desc = self.descriptor_mut(ids[0]);
            desc.flags |= VirtQueueDesc::FLAG_NEXT;
            desc.next = ids[1];
        }

        // register the descriptor chain in the available array
        // by storing the first descriptor id of the chain
        self.avail.ring[self.avail.idx as usize] = ret[0];
        self.avail.idx += 1;

        ret
    }

    fn get_free_desc_id(&mut self) -> VirtQueueDescId {
        let id = self.first_free_desc;
        self.first_free_desc = self.desc[usize::from(id)].next;
        id
    }
 
    pub fn free_desc_chain(&mut self, first: VirtQueueDescId) {
        let mut iter = first;
        loop {
            let first_free = self.first_free_desc;
            let desc = self.descriptor_mut(iter);
            let next = desc.next;
            let flags = desc.flags;
            desc.next = first_free;

            self.first_free_desc = iter;

            if (flags & VirtQueueDesc::FLAG_NEXT) == 0 { break }
            
            iter = next;
        }
    }

    pub fn free_one_desc(&mut self, did: VirtQueueDescId) {
        let first_free = self.first_free_desc;
        let desc = self.descriptor_mut(did);
        desc.next = first_free;
        self.first_free_desc = did;
    }

    pub fn is_looping(&self, id: VirtQueueDescId) -> bool {
        let mut iter = id;
        loop {
            let desc = self.descriptor(iter);
            let next = if (desc.flags & VirtQueueDesc::FLAG_NEXT) != 0 {
                Some(desc.next)
            } else {
                None
            };

            match next {
                None => break false,
                Some(next) => if next != id { iter = next } else { break true },
            }
        }
    }

    pub fn descriptor(&self, id: VirtQueueDescId) -> &VirtQueueDesc {
        &self.desc[usize::from(id)]
    }

    pub fn descriptor_mut(&mut self, id: VirtQueueDescId) -> &mut VirtQueueDesc {
        &mut self.desc[usize::from(id)]
    }

    pub fn desc(&self) -> &VirtQueueDesc {
        &self.desc[0]
    }

    pub fn avail(&self) -> &VirtQueueAvail<S> {
        &self.avail
    }

    pub fn used(&self) -> &VirtQueueUsed<S> {
        &self.used
    }

    pub fn first_used_descriptor(&self) -> VirtQueueDescId {
        VirtQueueDescId { id: self.used.ring[self.first_visible_used].id as u16 }
    }

    pub fn first_used_len(&self) -> usize {
        self.used.ring[self.first_visible_used].len as usize
    }

    pub fn clear_events(&mut self) {
        while self.first_visible_used != self.used.idx as usize {
            let id = self.used.ring[self.first_visible_used];
            self.free_desc_chain(VirtQueueDescId { id: id.id as u16 });
            self.first_visible_used += 1;
        }
    }

    pub fn print_infos(&self) {
        println!("size: {}", S);
        println!("first_free_desc: {}", self.first_free_desc.id);
        println!("first_visible_used: {}", self.first_visible_used);
        println!("avail.idx: {}", self.avail.idx);
        println!("used.idx: {}", self.used.idx);
    }

    pub fn consume_one_used(&mut self) -> usize {
        let ret = self.first_visible_used;
        self.first_visible_used += 1;
        ret
    }
}
