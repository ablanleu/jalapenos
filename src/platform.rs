//! This module gathers all addresses used by the kernel to use the machine properly.
//! Having them here instead of in each module using them allow us to modify the platform we work
//! on more easily, as we only need to modify this file for all memory base addresses.
//!
//! It also allows easy relocation (see crate::mem::virt) because 32bits addressing can be tricky
//! on new machines

/// RAM ORIGIN
pub static mut RAM_ORIGIN : usize = 0x8000_0000;

/// UART base address used by the `uart` module to read and write into the serial port
pub const UART_BASE_ADDR_CST : *mut u8 = 0x1000_0000 as *mut u8;
pub static mut UART_BASE_ADDR : *mut u8 = 0x1000_0000 as *mut u8;

/// PLIC registers base address used to handle external interrupts
pub static mut PLIC_BASE_ADDR : *mut u8 = 0x0C00_0000 as *mut u8;

/// Address of the most significant u32 of the u64 values of CSR mtime and mtimecmp
///
/// these memory-mapped registers are used for controling timer interrupt
pub static mut MTIMECMP_HI : *mut u32 = 0x0200_4000 as *mut u32;
pub static mut MTIME_HI : *mut u32 = 0x0200_BFF8 as *mut u32;

/// VirtIO MMIO protocol entry point
pub static mut VIRTIO : *mut u8 = 0x10001000 as *mut u8;
pub const VIRTIO_STRIDE : usize = 0x1000;
pub const VIRTIO_NUM : usize = 0x8;
pub const VIRTIO_SIZE : usize = VIRTIO_STRIDE * VIRTIO_NUM;

#[inline]
pub fn set_timeout(time:u32) {
    let mtimecmp_hi = unsafe { MTIMECMP_HI };
    let mtime_hi = unsafe { MTIME_HI };

    unsafe {
        let (lo, overflow) = mtime_hi
            .read_volatile()
            .overflowing_add(time);
        let hi = mtime_hi.add(1).read_volatile() + (overflow as u32);


        mtimecmp_hi.write_volatile(0xffffffff);
        mtimecmp_hi.add(1).write_volatile(hi);
        mtimecmp_hi.write_volatile(lo);
    }
}
