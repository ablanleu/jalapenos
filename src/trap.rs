use crate::{
    cpu,
    uart::Uart,
//    mem::mmu::{PageId, Phys, PTE},
    plic,
    platform,
    scheduler::{self, Thread},
    isa::Instruction,
    object::{Object, GLOBAL_LIST},
    descriptor::Desc,
};
use alloc::boxed::Box;

extern "C" {
    /// This function triggers an environment call from S-mode. For now the only reason we have it
    /// is to allow us to handle Timer interrupts in S-mode. Because STIP cannot be reset from
    /// S-mode, we need to call machine mode to do so.
    fn do_syscall();
}

#[no_mangle]
extern "C" fn s_trap_handler(sepc:usize, scause:usize, stval:usize, current: Option<Box<Thread>>) -> Option<Box<Thread>> {
    let code = scause & 0xfff;
    let is_interrupt = (scause & 0x8000_0000usize) != 0;

    let name : [&str;32] = [
        "Instruction address misaligned"
    ,   "Instruction access fault"
    ,   "Illegal instruction"
    ,   "Breakpoint"
    ,   "Load address misaligned"
    ,   "Load access fault"
    ,   "Store/AMO address misaligned"
    ,   "Store/AMO access fault"
    ,   "Environment call from U-mode"
    ,   "Environment call from S-mode"
    ,   "Unknown exception"
    ,   "Environment call from M-mode"
    ,   "Instruction page fault"
    ,   "Load page fault"
    ,   "Unknown exception"
    ,   "Store/AMO page fault"
    ,   "User software interrupt"
    ,   "Supervisor software interrupt"
    ,   "Hypervisor software interrupt"
    ,   "Machine software interrupt"
    ,   "User timer interrupt"
    ,   "Supervisor timer interrupt"
    ,   "Hypervisor timer interrupt"
    ,   "Machine timer interrupt"
    ,   "User external interrupt"
    ,   "Supervisor external interrupt"
    ,   "Hypervisor external interrupt"
    ,   "Machine external interrupt"
    ,   ""
    ,   ""
    ,   ""
    ,   ""
    ];

    let to_run = if is_interrupt {
        match code {
            // Timer interrupts
            5 => {
                unsafe {
                    let new = match current {
                        Some(mut old) => {
                            old.frame.pc = sepc;
                            Some(Thread::switch(old))
                        },
                        None => {
                            scheduler::schedule_next()
                        },
                    };

                    if new.is_none() && !scheduler::is_anyone_alive() {
                        panic!("oh, no thread is alive, you may want to shutdown the machine.");
                    }

                    // setup next time-slice
                    platform::set_timeout(cpu::FREQ);
                    do_syscall();

                    new
                }
            },
            // we normally will never have a machine mode external exception because we setup
            // the PLIC and we only enable interrupts in supervisor mode
            9 => { 
                if let Some(plic_inter_code) = plic::claim() {
                    let code = plic_inter_code.get();
                    match code {
                        10 => {
                            let c = Uart::new(unsafe { platform::UART_BASE_ADDR as *mut u8 }).get().unwrap();
                            match c {
                                10 | 13 => { println!() },
                                _ => { print!("{}", c as char) }
                            }
                        },
                        8 => { // VirtIO MMIO block device
                            crate::virtio::block::handle_request();
                        },
                        n => { println!("Unhandled PLIC interruption {}", n) },
                    }
                    plic::complete(code as u32);
                }

                current
            },
            _ => {
                println!("Unhandled interrupt: {}", name[(code + 16) as usize]);
                current
            }
        }
    } else {
        match code {
            2 => { // illegal instruction

                let inst = Instruction(unsafe { (sepc as *mut u32).read_volatile() });
                let is_wfi = inst.0 == 0x10500073;

                let new = match current {
                    None => { panic!("Illegal instruction in supervisor mode") },
                    Some(mut old) => {
                        if is_wfi {
                            //old.frame.pc += 4;
                            Some(old)
                        } else {
                            scheduler::kill_thread(old)
                        }
                    },
                };

                new
            },
            12 => {
                match current {
                    Some(current) => {
                        if stval == 0xfffffffe {
                            scheduler::kill_thread(current)
                        } else {
                            panic!("Instruction page fault on user thread but not at 'stop' address");
                        }
                    },
                    None => {
                        panic!("Instruction page fault in supervisor mode")
                    },
                }
            },
            13 | 15 => {
                panic!("Exception {}\n\tsepc {:x}\n\tstval {:x}", name[code], sepc, stval)
            },
            8 => {
                let mut thread = current.expect("Syscall from supervisor mode ?");
                thread.frame.pc = sepc + 4;
                handle_syscall(thread)
            },
            _ => {
                current
            },
        }
    };

    // if we have a thread to run, make sure we set SATP to the right value
    if let Some(thread) = to_run.as_ref() {
        let satp = thread.process.spin_lock().satp;
        cpu::set_satp(satp as u32);
    }

    to_run
}

/// Please make sure this enum is commented the right way
/// all the comments following enum's values are here to tell a little script (build_c_user_lib.py)
/// how to generate a C file containing syscall functions to use in user-mode programs
#[repr(usize)]
enum SyscallCode {
    Puts = 0,   // void(const char*, int)
    PageAlloc,  // void*(int)
    BlkRead,    // void(const char*, int, int)
    BlkWrite,   // void(const char*, int, int)
    Fork,       // int(void(*)(void*), void*)
    Join,       // void(int)
    SemaCreate, // int(int)
    SemaFree,   // void(int)
    SemaP,      // void(int)
    SemaV,      // void(int)
    LockCreate, // int(void)
    LockFree,   // void(int)
    LockLock,   // void(int)
    LockUnlock, // void(int)
}

impl SyscallCode {
    fn from_usize(id: usize) -> Option<Self> {
        if id <= Self::LockUnlock as usize {
            Some(unsafe { core::mem::transmute::<usize, Self>(id) })
        } else {
            None
        }
    }
}

// This function is here to handle a syscall from usermode.
// When called, its parameter is the thread responsible for the syscall. Its frame contains the
// parameters of the syscall. RISC-V's ABI says that first parameters are contained in the 
// a0-a6 registers and specificaly for syscalls a7 is used to contain the syscall's code.
fn handle_syscall(mut thread: Box<Thread>) -> Option<Box<Thread>> {
    // load a7 register containing the syscall's identifier
    let code = SyscallCode::from_usize(thread.frame.iregs[16]).expect("bad syscall code");

    match code {
        SyscallCode::Puts => { // puts
            // a1 and a2 contain a string and its length, we use them to retrieve a &str and print
            // it
            let ptr = thread.frame.iregs[9] as *mut u8;
            let len = thread.frame.iregs[10] as usize;
            let slice = unsafe { core::slice::from_raw_parts(ptr, len) };
            let s = core::str::from_utf8(slice).unwrap();

            print!("{}", s);

            Some(thread)
        },
        SyscallCode::PageAlloc => { // page alloc
            {
                let mut process = thread.process.spin_lock();
                let size = thread.frame.iregs[9] as usize;
                let begin_address = process.allocate_heap_pages(size);

                thread.frame.iregs[9] = begin_address;
            }

            Some(thread)
        },
        SyscallCode::BlkRead => { // read from block device
            let dst = thread.frame.iregs[9];
            let src = thread.frame.iregs[10];
            let len = thread.frame.iregs[11];

            let slice = unsafe { core::slice::from_raw_parts_mut(dst as *mut u8, len) };
            crate::virtio::block::read_syscall(thread, src, slice);

            let thread = scheduler::schedule_next();
            thread
        },
        SyscallCode::BlkWrite => { // read from block device
            let dst = thread.frame.iregs[9];
            let src = thread.frame.iregs[10];
            let len = thread.frame.iregs[11];

            let slice = unsafe { core::slice::from_raw_parts_mut(src as *mut u8, len) };
            crate::virtio::block::write_syscall(thread, dst, slice);

            let thread = scheduler::schedule_next();
            thread
        },
        SyscallCode::Fork => { // fork
            let func_ptr = thread.frame.iregs[9];

            let frame = {
                let mut process = thread.process.spin_lock();
                let stack_ptr = process.make_stack();

                let mut frame = crate::process::Frame {
                    pc: func_ptr,
                    iregs: [0; 31],
                };
                frame.iregs[0] = 0xfffffffe;
                frame.iregs[1] = stack_ptr;
                frame.iregs[7] = stack_ptr;
                frame.iregs[2] = process.gp;
                frame
            };

            let sp = frame.iregs[1];
            let th = Thread::new(frame, thread.process.clone(), sp);
            
            let id = GLOBAL_LIST.spin_lock().insert(Object::Thread(Desc::of(th.as_ref())));
            Thread::wakeup(th);

            thread.frame.iregs[9] = id;
            Some(thread)
        },
        SyscallCode::Join => { // join
            let oid = thread.frame.iregs[9];
            let list = GLOBAL_LIST.spin_lock();
            let obj = list.get(oid);

            match obj {
                Some(Object::Thread(ptr)) => {
                    Thread::join(thread, *ptr)
                },
                _ => {
                    Some(thread)
                },
            }
        },
        SyscallCode::SemaCreate => {
            let capa = thread.frame.iregs[9];
            let mut list = GLOBAL_LIST.spin_lock();
            let oid = list.insert(Object::Semaphore(crate::sync::Semaphore::new(capa)));

            thread.frame.iregs[9] = oid;
            Some(thread)
        },
        SyscallCode::SemaFree => {
            let oid = thread.frame.iregs[9];
            let mut list = GLOBAL_LIST.spin_lock();


            enum SemaFreeResult {
                NotASema,
                SemaNotFree,
            }

            // TODO: think about a better API for using the object list.
            let res = list.check_remove(oid, |obj| {
                if let Object::Semaphore(sema) = obj {
                    if sema.is_free() {
                        None
                    } else {
                        Some(SemaFreeResult::SemaNotFree)
                    }
                } else {
                    Some(SemaFreeResult::NotASema)
                }
            });

            match res {
                None => { /* TODO: no object found */ },
                Some(Err(SemaFreeResult::SemaNotFree)) => { /* TODO: SemaNotFree */ },
                Some(Err(SemaFreeResult::NotASema)) => { /* TODO: NotASema */ },
                Some(Ok(sema)) => { /* TODO: do the operation */ },
            }
            
            Some(thread)
        },
        SyscallCode::SemaP => {
            let oid = thread.frame.iregs[9];
            let mut list = GLOBAL_LIST.spin_lock();

            match list.get_mut(oid) {
                Some(Object::Semaphore(sema)) => {
                    sema.P(thread)
                },
                _ => {
                    // TODO: errors
                    Some(thread)
                },
            }
        },
        SyscallCode::SemaV => {
            let oid = thread.frame.iregs[9];
            let mut list = GLOBAL_LIST.spin_lock();

            match list.get_mut(oid) {
                Some(Object::Semaphore(sema)) => {
                    sema.V();
                },
                _ => {
                    // TODO: errors
                },
            }
            Some(thread)
        },
        _ => {
            panic!("unimplemented syscall")
        },
    }
}
