use crate::{
    scheduler::Thread,
    descriptor::Desc,
};
use alloc::{boxed::Box, collections::vec_deque::VecDeque};

pub struct Semaphore {
    n: usize,
    waiting_threads: VecDeque<Box<Thread>>,
}

impl Semaphore {
    pub fn new(n: usize) -> Self {
        Self {
            n,
            waiting_threads: VecDeque::new(),
        }
    }

    pub fn P(&mut self, thread: Box<Thread>) -> Option<Box<Thread>> {
        if self.n == 0 {
            self.waiting_threads.push_back(thread);
            None
        } else {
            self.n -= 1;
            Some(thread)
        }
    }

    pub fn V(&mut self) {
        self.n += 1;
        self.waiting_threads
            .pop_front()
            .map(|thread| {
                Thread::wakeup(thread)
            });
    }

    pub fn is_free(&self) -> bool {
        self.waiting_threads.is_empty()
    }
}

pub struct Lock {
    owner: Option<Desc<Thread>>,
    waiting_threads: VecDeque<Box<Thread>>,
}

impl Lock {
    pub fn new() -> Self {
        Self {
            owner: None,
            waiting_threads: VecDeque::new(),
        }
    }

    pub fn lock(&mut self, thread: Box<Thread>) -> Option<Box<Thread>> {
        match self.owner.take() {
            None => {
                self.owner = Some(Desc::of(&thread));
                Some(thread)
            },
            Some(th) => {
                self.owner = Some(th);
                if th == Desc::of(&thread) {
                    Some(thread)
                } else {
                    self.waiting_threads.push_back(thread);
                    None
                }
            },
        }
    }

    pub fn unlock(&mut self, thread: &Box<Thread>) {
        match self.owner.take() {
            None => {
                panic!("unlock non locked lock ?");
            },
            Some(th) => {
                if Desc::of(thread.as_ref()) == th {
                    self.waiting_threads.pop_front().map(Thread::wakeup);
                } else {
                    panic!("unlock from a thread which does not own it");
                }
            },
        }
    }
}
