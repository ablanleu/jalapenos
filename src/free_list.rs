use alloc::vec::Vec;

pub struct FreeList {
    upper: usize,
    list: Vec<usize>,
}

impl FreeList {
    pub fn new() -> Self {
        Self { upper: 0, list: Vec::new() }
    }

    pub fn get(&mut self) -> usize {
        self.list.pop().unwrap_or_else(|| {
            let ret = self.upper;
            self.upper += 1;
            ret
        })
    }

    pub fn free(&mut self, id: usize) {
        if id + 1 == self.upper {
            self.upper -= 1;
        } else {
            for i in 0..self.list.len() {
                if self.list[i] == id {
                    self.list.swap_remove(id);
                    return;
                }
            }
        }
    }
}
