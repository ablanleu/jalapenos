
use core::fmt::{Write, Result as IOResult};
use crate::platform::UART_BASE_ADDR;

#[macro_export]
macro_rules! print {
    ($($args:tt)+) => ({
        use core::fmt::Write;
        use crate::uart::Uart;
        #[allow(unused_unsafe)]
        let _ = write!(Uart::new(unsafe { crate::platform::UART_BASE_ADDR }), $($args)+);
    })
}

#[macro_export]
macro_rules! println {
    () => { print!("\r\n") };
    ($fmt:expr) => {
        print!(concat!($fmt, "\r\n"))
    };
    ($fmt:expr, $($args:tt)+) => {
        print!(concat!($fmt, "\r\n"), $($args)+)
    }
}

#[cfg(debug_assertions)]
#[macro_export]
macro_rules! dbg {
    ($($args:tt)+) => ({
        println!($($args)+);
    })
}

#[cfg(not(debug_assertions))]
#[macro_export]
macro_rules! dbg {
    ($($args:tt)+) => ({
    })
}

#[repr(transparent)]
pub struct Uart {
    base: *mut u8,
}

impl Uart {
    pub fn new(base: *mut u8) -> Uart {
        Uart { base }
    }

    pub fn put(&mut self, c: u8) {
        unsafe {
            self.base.write_volatile(c);
        }
    }

    pub fn get(&mut self) -> Option<u8> {
        unsafe {
            let pending = self.base.add(5).read_volatile();
            if (pending & 1) == 1 {
                Some(self.base.read_volatile())
            } else {
                None
            }
        }
    }

    pub fn init() {
        unsafe {
            let ptr = UART_BASE_ADDR as *mut u8;
            // UART word width == 8bits
            ptr.add(3).write_volatile(0b11);

            // enable FIFO
            ptr.add(2).write_volatile(0b1);

            // enable RX interrupts
            ptr.add(1).write_volatile(0b1);
	    }
    }
}

impl Write for Uart {
    fn write_str(&mut self, s:&str) -> IOResult {
        for c in s.bytes() {
            self.put(c);
        }
        Ok(())
    }
}
