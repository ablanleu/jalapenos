use crate::{
    process::{Process, Frame, STACK_SIZE},
    lock::Mutex,
    lazy::LazySyncCell,
    mem::{PAGE_SIZE, mmu::PageId},
    descriptor::Desc,
};
use alloc::{
    sync::Arc,
    collections::{vec_deque::VecDeque, btree_map::BTreeMap},
    boxed::Box,
};

struct ThreadQueue {
    running_queue: VecDeque<Box<Thread>>,
    join_queue: JoinQueue,
}

type JoinQueue = BTreeMap<Desc<Thread>, VecDeque<Box<Thread>>>;

static STATE : LazySyncCell<Mutex<ThreadQueue>> = LazySyncCell::new(|| {
    Mutex::new(ThreadQueue {
        running_queue: VecDeque::new(),
        join_queue: BTreeMap::new(),
    })
});

#[repr(C)]
pub struct Thread {
    pub frame: Frame,
    pub process: Arc<Mutex<Process>>,
    pub stack_top: usize,
}

impl Thread {
    pub fn new(frame: Frame, process: Arc<Mutex<Process>>, stack_top: usize) -> Box<Thread> {
        let ret = Box::new(Thread {
            frame,
            process,
            stack_top,
        });
        STATE.spin_lock().join_queue.insert(Desc::of(&ret), VecDeque::new());
        ret
    }

    pub fn make_ready(this: Box<Self>) {
        let mut st = STATE.spin_lock();
        st.running_queue.push_back(this);
    }

    pub fn wakeup(this: Box<Self>) {
        Self::make_ready(this);
    }

    pub fn switch(this: Box<Self>) -> Box<Self> {
        let mut st = STATE.spin_lock();
        st.running_queue.push_back(this);
        st.running_queue.pop_front().unwrap()
    }

    pub fn join(this: Box<Self>, to_join: Desc<Thread>) -> Option<Box<Self>> {
        if is_running(to_join) {
            let mut st = STATE.spin_lock();
            st.join_queue
                .entry(to_join)
                .or_insert(VecDeque::new())
                .push_back(this);
            st.running_queue.pop_front()
        } else {
            Some(this)
        }
    }
}

impl core::ops::Drop for Thread {
    fn drop(&mut self) {
        let proc = self.process.spin_lock();
        let stack_page = PageId::from_address(self.stack_top - 1);

        for i in 0..(STACK_SIZE / PAGE_SIZE) {
            proc.address_space().unmap(stack_page - i);
        }

        let mut st = STATE.spin_lock();
        st.join_queue
            .remove(&Desc::of(self))
            .map(|mut l| {
                while let Some(thread) = l.pop_back() {
                    st.running_queue.push_back(thread);
                }
            });
    }
}

pub fn schedule_next() -> Option<Box<Thread>> {
    let mut st = STATE.spin_lock();
    st.running_queue.pop_front()
}

pub fn kill_thread(to_kill: Box<Thread>) -> Option<Box<Thread>> {
    drop(to_kill);
    let mut st = STATE.spin_lock();
    st.running_queue.pop_front()
}

pub fn is_running(thread: Desc<Thread>) -> bool {
    let st = STATE.spin_lock();
    st.join_queue.keys().find(|th| thread == **th).is_some()
}

pub fn is_anyone_alive() -> bool {
    let st = STATE.spin_lock();
    !st.join_queue.is_empty()
}
