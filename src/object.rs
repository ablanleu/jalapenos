use crate::{
    scheduler::Thread,
    sync::{Semaphore, Lock},
    lazy::LazySyncCell,
    lock::Mutex,
    free_list::FreeList,
    descriptor::Desc,
};
use alloc::collections::btree_map::BTreeMap;

pub enum Object {
    Thread(Desc<Thread>),
    Semaphore(Semaphore),
    Lock(Lock),
}

pub struct GlobalObjectList {
    map: BTreeMap<usize, Object>,
    free_list: FreeList,
}

impl GlobalObjectList {
    pub fn get(&self, id: usize) -> Option<&Object> {
        self.map.get(&id)
    }

    pub fn get_mut(&mut self, id: usize) -> Option<&mut Object> {
        self.map.get_mut(&id)
    }

    pub fn remove(&mut self, id: usize) -> Option<Object> {
        self.free_list.free(id);
        self.map.remove(&id)
    }

    pub fn insert(&mut self, obj: Object) -> usize {
        let id = self.free_list.get();
        self.map.insert(id, obj);
        id
    }

    pub fn check_remove<E, F:FnMut(&Object) -> Option<E>>(&mut self, id: usize, mut f: F) -> Option<Result<Object, E>> {
        let obj = self.map.remove(&id)?;
        let e = f(&obj);
        match e {
            None => {
                Some(Ok(obj))
            },
            Some(e) => {
                self.map.insert(id, obj);
                Some(Err(e))
            },
        }
    }
}

pub static GLOBAL_LIST : LazySyncCell<Mutex<GlobalObjectList>> = LazySyncCell::new(|| {
    Mutex::new(GlobalObjectList {
        map: BTreeMap::new(),
        free_list: FreeList::new(),
    })
});

