use crate::{
    mem::{phys, mmu},
};
use plain::{Plain, Error as PlainError};
use goblin::{
    elf32::{
        header::*,
        program_header::*,
        section_header::*,
    },
    strtab,
};

pub fn load_elf(file_content: &[u8]) -> Result<(usize, mmu::PageId<mmu::Phys>), PlainError> {
    let hd : &Header = Plain::from_bytes(&file_content[..])?;
    let phs = ProgramHeader::slice_from_bytes(&file_content[hd.e_phoff as usize..])?;
    let shs = SectionHeader::slice_from_bytes(&file_content[hd.e_shoff as usize..])?;

    let mut first_byte = 0xffffffffusize;
    let mut last_byte = 0usize;

    for ph in phs[..hd.e_phnum as usize].iter() {
        if ph.p_type == 1 {
            first_byte = first_byte.min(ph.p_vaddr as usize);
            last_byte  = last_byte.max(ph.p_vaddr as usize + ph.p_memsz as usize);
        }
    }

    let size = last_byte - first_byte;
    let nb_pages = size / 4096;

    let pid = phys::alloc_pages(nb_pages);

    for i in 0..nb_pages {
        let pp = pid + i;

    }

    unsafe {
        for ph in phs[..hd.e_phnum as usize].iter() {
            if ph.p_type == 1 {
                
            }
        }
    }

    Ok((hd.e_entry as usize, pid))
}
