use core::{
    sync::atomic::{Ordering, AtomicBool},
    ops::{Deref, DerefMut, Drop},
    cell::UnsafeCell,
    mem::MaybeUninit,
};

pub struct Mutex<T> {
    data: UnsafeCell<T>,
    locked: AtomicBool,
}

impl<T> Mutex<T> {
    pub const fn new(data: T) -> Self {
        Self {
            locked: AtomicBool::new(false),
            data: UnsafeCell::new(data),
        }
    }

    pub fn try_lock<'a>(& 'a self) -> Option<MutexLock<'a, T>> {
        if self.locked.swap(true, Ordering::AcqRel) {
            None
        } else {
            Some(MutexLock { mtx: self })
        }
    }

    pub fn spin_lock<'a>(& 'a self) -> MutexLock<'a, T> {
        loop {
            match self.try_lock() {
                Some(lock) => break lock,
                None => {},
            }
        }
    }

    fn _unlock(&self) {
        self.locked.store(false, Ordering::Release);
    }

    pub const fn uninit() -> Mutex<MaybeUninit<T>> {
        Mutex::new(MaybeUninit::uninit())
    }
}

// SAFETY: I DONT KNOW I TOOK IT FROM THE STANDARD LIBRARY
unsafe impl<T> Send for Mutex<T> where T : Send {}
unsafe impl<T> Sync for Mutex<T> where T : Send {}

pub struct MutexLock<'a, T> {
    mtx: & 'a Mutex<T>,
}

impl<'a, T> Deref for MutexLock<'a, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        unsafe { self.mtx.data.get().as_ref().unwrap() }
    }
}

impl<'a, T> DerefMut for MutexLock<'a, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { self.mtx.data.get().as_mut().unwrap() }
    }
}

impl<'a, T> Drop for MutexLock<'a, T> {
    fn drop(&mut self) {
        self.mtx._unlock()
    }
}

