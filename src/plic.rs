use crate::platform::PLIC_BASE_ADDR;
use core::num::NonZeroU8;

pub fn enable(id:u32) {
    unsafe {
        let ptr = PLIC_BASE_ADDR.add(0x2080) as *mut u32; // Hart 0 Supervisor
        ptr.write_volatile(ptr.read_volatile() | (1 << id));
    }
}

pub fn set_priority(id:u32, prio:u32) {
    let actual_prio = prio & 0b111;
    unsafe {
        let ptr = (PLIC_BASE_ADDR as *mut u32).add(id as usize);
        ptr.write_volatile(actual_prio)
    }
}

pub fn set_threshold(tsh: u32) {
    let actual_tsh = tsh & 0b111;
    unsafe {
        let tsh_reg = PLIC_BASE_ADDR.add(0x20_1000) as *mut u32; // Hart 0 Supervisor
        tsh_reg.write_volatile(actual_tsh);
    }
}

pub fn claim() -> Option<NonZeroU8> {
    unsafe {
        let ptr = PLIC_BASE_ADDR.add(0x20_1004) as *mut u32; // Hart 0 Supervisor
        let id = ptr.read_volatile();

        NonZeroU8::new(id as u8)
    }
}

pub fn complete(id:u32) {
    unsafe {
        let ptr = PLIC_BASE_ADDR.add(0x20_1004) as *mut u32; // Hart 0 Supervisor
        ptr.write_volatile(id);
    }
}
