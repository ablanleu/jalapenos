use core::ptr::null_mut;
use crate::{
    mem::{alloc, mmu::{PageId, Table, Virt, PTE}},
    platform::*,
};

pub static mut FIRST_USER_PAGE : PageId<Virt> = PageId::from_id(0);
pub static mut KERNEL_VIRTUAL_SPACE : *mut Table = null_mut();

/// This stage modifies the addresses used by the kernel from their physical value to a well chosen
/// virtual one. We do this to pack kernel virtual space in low memory. Packing in low
/// memory removes memory fragmentation and allow us to split virtual space into 2 contiguous
/// regions for kernel and user space.
pub fn init(kernel_mega_pages: usize) -> *mut Table {
    unsafe {
        let root = alloc::alloc_page_table().as_mut().expect("Couldn't allocate a page table");

        println!("\t- Map kernel ({} mega pages)", kernel_mega_pages);
        // map kernel memory starting from virtual address 0
        for i in 0..kernel_mega_pages {
            let offset = 0x0040_0000 * i;
            let paddr = 0x8000_0000 + offset;
            let vaddr = paddr;// offset;
            let pkernel = PageId::from_address(paddr);
            let vkernel = PageId::from_address(vaddr);

            root.map(vkernel, pkernel, 1, PTE::PERMANENT | PTE::READ | PTE::WRITE | PTE::EXECUTE);
            assert_eq!(Some(paddr), root.virt_to_phys(vaddr).map(|x| x as usize));
        }

        const BASE_MMIO : usize = 0xC000_0000;

        println!("\t- Map plic");
        // map plic mega page just after kernel
        let old = PLIC_BASE_ADDR as u64;
        let pplic = PageId::from_address(PLIC_BASE_ADDR as usize);
        let vplic = PageId::from_address(BASE_MMIO);
        root.map(vplic, pplic, 1, PTE::PERMANENT | PTE::READ | PTE::WRITE);
        PLIC_BASE_ADDR =
            (((PLIC_BASE_ADDR as usize) & 0xfff) + (vplic.to_64_address() as usize)) as *mut u8;
        assert_eq!(Some(old), root.virt_to_phys(PLIC_BASE_ADDR as usize));
        println!("\t  + plic virtual base is {:08x}", PLIC_BASE_ADDR as usize);


        // the rest of the pages can be mapped as simple pages
        let simple_page_start = vplic + 1024;

        println!("\t- Map mtimecmp");
        let old = MTIMECMP_HI as u64;
        let pmtimecmp = PageId::from_address(MTIMECMP_HI as usize);
        let vmtimecmp = simple_page_start;
        root.map(vmtimecmp, pmtimecmp, 0, PTE::PERMANENT | PTE::READ | PTE::WRITE);
        MTIMECMP_HI = ((MTIMECMP_HI as usize & 0xfff) + (vmtimecmp.to_64_address() as usize)) as *mut u32;
        println!("old={:x} ; new={:x}", old, MTIMECMP_HI as usize);
        assert_eq!(Some(old), root.virt_to_phys(MTIMECMP_HI as usize));
        println!("\t  + mtimecmp virtual base is {:08x}", MTIMECMP_HI as usize);

        println!("\t- Map mtime");
        let old = MTIME_HI as u64;
        let pmtime = PageId::from_address(MTIME_HI as usize);
        let vmtime = simple_page_start + 1;
        root.map(vmtime, pmtime, 0, PTE::PERMANENT | PTE::READ | PTE::WRITE);
        MTIME_HI = ((MTIME_HI as usize & 0xfff) + (vmtime.to_64_address() as usize)) as *mut u32;
        assert_eq!(Some(old), root.virt_to_phys(MTIME_HI as usize));
        println!("\t  + mtime virtual base is {:08x}", MTIME_HI as usize);

        println!("\t- Map VirtIO");
        let old = VIRTIO as u64;
        for i in 0..VIRTIO_NUM {
            let pvirtio = PageId::from_address(VIRTIO as usize + i * VIRTIO_STRIDE);
            let vvirtio = simple_page_start + 2 + i;
            root.map(vvirtio, pvirtio, 0, PTE::PERMANENT | PTE::READ | PTE::WRITE);
        }
        VIRTIO = ((VIRTIO as usize & 0xfff) + (usize::from(simple_page_start + 2) << 12)) as *mut u8;
        assert_eq!(Some(old), root.virt_to_phys(VIRTIO as usize));

        println!("\t- Map uart /!\\ NO MORE TEXT UNTIL KERNEL RELOCATION /!\\");
        let old = UART_BASE_ADDR as u64;
        let puart = PageId::from_address(UART_BASE_ADDR as usize);
        let vuart = simple_page_start + 2 + VIRTIO_NUM;
        root.map(vuart, puart, 0, PTE::PERMANENT | PTE::READ | PTE::WRITE);

        // PAST THIS LINE RELOCATION NEEDS TO BE DONE ENTIRELY BEFORE RE-USING println!
        UART_BASE_ADDR = ((UART_BASE_ADDR as usize & 0xfff) + (vuart.to_64_address() as usize)) as *mut u8;

        if Some(old) != root.virt_to_phys(UART_BASE_ADDR as usize) {
            UART_BASE_ADDR = old as *mut u8;
            panic!("Relocation of UART didnt work")
        }
        //assert_eq!(Some(old), root.virt_to_phys(UART_BASE_ADDR as usize));

        FIRST_USER_PAGE = PageId::from_id(0);

        super::alloc::reloc();
        super::phys::reloc();
        super::mmu::reloc();

        KERNEL_VIRTUAL_SPACE = root;
        root
    }
}
