use super::{mmu::{PageId, Phys}, PAGE_SIZE};
use core::{
    mem::size_of,
    num::NonZeroUsize,
};

#[inline]
pub fn align_floor(value: usize, order: usize) -> usize {
    let mask = (1 << order) - 1;
    value & !mask
}

#[inline]
pub fn align_ceil(value: usize, order: usize) -> usize {
    let mask = (1 << order) - 1;
    (value + mask) & !mask
}

extern "C" {
    pub static HEAP_START : usize;
    pub static HEAP_SIZE : usize;
}

/// Memory right after the kernel stacks is used for dynamic allocation. It is composed of different
/// statically defined kernel zones.
///
/// The first of these zones is the ZERO_PAGE. This page is the physical page to which every virtual
/// process pages point to when allocated. This page is write protected, such that all reads to it
/// will always work (and read the value 0), but any write (including the very first) will trigger a
/// page fault which will launch the physical page allocation routine.
///
/// Next comes the physical page descriptors list which tells the kernel which physical page is
/// used or free.
///
/// After that, all pages are free for the kernel to allocate

pub struct MemoryLayout {
    page_descriptors_start: NonZeroUsize,
    first_allocable_page: NonZeroUsize,
    nb_allocable_pages: usize,
}

impl MemoryLayout {
    fn new(desc: usize, alloc: usize, nb_alloc: usize) -> Option<Self> {
        Some(Self {
            page_descriptors_start: NonZeroUsize::new(desc)?,
            first_allocable_page: NonZeroUsize::new(alloc)?,
            nb_allocable_pages: nb_alloc,
        })
    }

    fn page_descriptors(&self) -> *mut PageInfo {
        (usize::from(self.page_descriptors_start) << 12) as *mut PageInfo
    }

    fn alloc_start(&self) -> usize {
        usize::from(self.first_allocable_page)
    }

    fn total_pages(&self) -> usize {
        self.nb_allocable_pages
    }
}

/// Our global MemoryLayout
pub static mut LAYOUT : Option<MemoryLayout> = None;

/// This function initializes the OS's memory layout
pub fn init() {
    unsafe {
        // PAGE_DESCRIPTORS_START comes next
        let page_descriptors_start = align_ceil(HEAP_START, 12) / PAGE_SIZE;

        // Init all the descriptors
        let desc_per_page = PAGE_SIZE / size_of::<PageInfo>();

        let mut nb_alloc_pages = (HEAP_SIZE / PAGE_SIZE) - 1;
        let nb_desc_pages = nb_alloc_pages / desc_per_page;

        nb_alloc_pages -= nb_desc_pages;

        let ptr = (page_descriptors_start << 12) as *mut PageInfo;

        for i in 0..nb_alloc_pages {
            ptr.add(i).write_volatile(PageInfo::new_free())
        }
        //(*ptr.add(nb_alloc_pages - 1)).flags |= PageFlag::Last as u8;
        
        let first_allocable_page = page_descriptors_start + nb_desc_pages;

        LAYOUT = MemoryLayout::new(page_descriptors_start, first_allocable_page, nb_alloc_pages);
    }
}

/// Helper function to fetch the global MemoryLayout. It will fail if `phys::init()` was not called
/// before.
pub fn layout() -> & 'static MemoryLayout {
    unsafe { (&LAYOUT).as_ref().expect("phys::init() was not yet called") }
}

pub fn reloc() {
//    unsafe {
//        let layout = LAYOUT.as_mut().unwrap();
//
//        layout.page_descriptors_start
//            = NonZeroUsize::new(usize::from(layout.page_descriptors_start) & (0x7fffffff >> 12)).unwrap();
//    }
}

#[repr(u8)]
enum PageFlag {
    Taken = 1 << 0,
    Last = 1 << 1,
}

struct PageInfo {
    flags: u8,
}

impl PageInfo {
    fn new_free() -> Self {
        Self { flags: 0 }
    }

    fn free(&mut self) {
        self.flags = 0;
    }

    fn set_flag(&mut self, flag: PageFlag) {
        self.flags |= flag as u8;
    }

    fn unset_flag(&mut self, flag: PageFlag) {
        self.flags &= !(flag as u8);
    }

    fn is_free(&self) -> bool {
        (self.flags & PageFlag::Taken as u8) == 0
    }

    fn is_last(&self) -> bool {
        (self.flags & PageFlag::Last as u8) != 0
    }
}

pub fn alloc_pages(nb_pages: usize) -> PageId<Phys> {
    unsafe {
        let layout = layout();
        let ptr = layout.page_descriptors();
        let alloc_start = layout.alloc_start();

        let mut i = 0;

        loop {
            if (*ptr.add(i)).is_free() {
                let mut found = true;
                for j in 0..nb_pages {
                    if !(*ptr.add(i + j)).is_free() {
                        found = false;
                        i += j;
                        break;
                    }
                }

                if found {
                    for i in i..i+nb_pages {
                        (*ptr.add(i)).set_flag(PageFlag::Taken)
                    }
                    (*ptr.add(i + nb_pages - 1)).set_flag(PageFlag::Last);

                    break PageId::from_id(alloc_start + i);
                }
            }

            if i >= layout.total_pages() {
                break PageId::from_id(0);
            }

            i += 1;
        }
    }
}

pub fn free_pages(page: PageId<Phys>) {
    unsafe {
        let layout = layout();
        let mut idx = (page - layout.alloc_start()).into();
        let first_desc = layout.page_descriptors();

        loop {
            let desc = first_desc.add(idx);
            let was_last = (*desc).is_last();

            (*desc).free();

            if was_last {
                break;
            }

            if idx == layout.total_pages() {
                println!("Reached end of page table while freeing, there is an allocation bug");
                break
            }

            idx += 1;
        }
    }
}

pub fn print_page_allocation() {
    unsafe {
        let layout = layout();
        let ptr = layout.page_descriptors();
        let alloc_start = layout.alloc_start();
        let max = layout.total_pages();

        println!("Allocable zone: 0x{:08x} - 0x{:08x} ({} pages)", alloc_start * 4096, (alloc_start + max) * 4096 - 1, max);

        let mut i = 0;
        while i < max {
            let entry : PageInfo = ptr.add(i).read();
            if !entry.is_free() {
                let mut j = i;
                while j < max {
                    let entry : PageInfo = ptr.add(j).read();
                    if entry.is_last() {
                        break;
                    }
                    j += 1;
                }
                println!("[0x{:08x}; 0x{:08x}]", (i + alloc_start) * 4096, (j + alloc_start + 1) * 4096 - 1);
                i = j;
            }

            i += 1;
        }
    }
}
