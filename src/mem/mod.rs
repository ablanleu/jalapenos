/// provides kernel memory allocation primitives (page table, and fine grained allocation)
pub mod alloc;
/// provides Page Table structures for easy address-space management
pub mod mmu;
/// provides physical memory mapping and physical page allocation
pub mod phys;
/// provides virtual memory mapping
pub mod virt;

pub const PAGE_SIZE : usize = 4096;

pub fn init() {
    dbg!("\t- Init page allocator");
    phys::init();
    dbg!("\t- Init kernel allocator");
    let n = alloc::init();
    dbg!("\t- Init virtual memory");
    virt::init(n);
}
