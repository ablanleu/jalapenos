use core::{
    ptr::null_mut,
    slice::from_raw_parts_mut,
    alloc::{
        GlobalAlloc,
        Layout,
    },
    mem::{size_of, align_of},
};
use crate::{
    mem::{phys, mmu::{Table, PageId, Phys}},
};

const NB_KERNEL_HEAP_PAGES : usize = 2048;
const TOTAL_PAGE_TABLES : usize = 2048;

struct KernelAllocator {
    zero_page: PageId<Phys>,
    kmalloc_head: *mut AllocChunk,
    pt_head: *mut Table,
}

impl KernelAllocator {
    const fn uninit() -> Self {
        Self { zero_page: PageId::from_id(0), kmalloc_head: null_mut(), pt_head: null_mut() }
    }
}

unsafe impl GlobalAlloc for KernelAllocator {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        if self.kmalloc_head == null_mut() { return null_mut() }
        let align = layout.align().max(4);

        let (size, padding) = {
            let full_size = layout.size() + size_of::<AllocChunk>();
            let final_size = (full_size + (align-1)) & !(align-1);
            let padding = final_size - full_size;
            let final_size = layout.size() + size_of::<AllocChunk>() + padding;

            (final_size, padding)
        };

        let mut head = self.kmalloc_head;

        while (*head).data != 0 {
            if !(*head).is_taken() && (*head).get_size() >= size {
                break
            } else {
                let next = (*head).get_next();
                head = next;
            }
        }

        if (*head).data == 0 {

            // compute maximum left size
            let max_size = self.kmalloc_head as usize + NB_KERNEL_HEAP_PAGES * 4096 - head as usize;

            // no space left, we cannot allocate the memory zone
            if size >= max_size {
                return null_mut();
            }

            (*head).set_size(size);
            let next = (*head).get_next();
            (*next).data = 0;
        } else {
        }

        (*head).take();

        let ret = (head.add(1) as *mut u8).add(padding);

//        println!("[ALLOC] ALLOC FINISHED");
//        self.print_list();

        ret
    }

    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
//        println!("[ALLOC] FREE(0x{:08x})", ptr as usize);
        let align = layout.align().max(4);
        let full_size = size_of::<AllocChunk>() + layout.size();
        let final_size = (full_size + (align-1)) & !(align-1);
        let padding = final_size - full_size;
        let cs = size_of::<AllocChunk>() + padding;

        let chunk = ptr.sub(cs) as *mut AllocChunk;


        let next = (*chunk).get_next();
        if (*next).data == 0 {
            (*chunk).data = 0;
        } else {
            (*chunk).free()
        }

//        println!("[ALLOC] FREE FINISHED");
//        self.print_list();
    }

}

impl KernelAllocator {
    unsafe fn print_list(&self) {
        let mut head = self.kmalloc_head;
        while (*head).data != 0 {
            println!("[ALLOC] Chunk at {:08x}; size={}; taken={}", head as usize, (*head).get_size(), (*head).is_taken());
            head = (*head).get_next();
        }
    }
}
#[global_allocator]
static mut GA : KernelAllocator = KernelAllocator::uninit();

pub fn alloc_page_table() -> *mut Table {
    unsafe {
        //let head : &mut [Table] = from_raw_parts_mut(GA.pt_head, TOTAL_PAGE_TABLES);
        let head : *mut Table = GA.pt_head;//from_raw_parts_mut(GA.pt_head, TOTAL_PAGE_TABLES);
        for i in 0..TOTAL_PAGE_TABLES {//tbl in head {
            let tbl = head.add(i);
            if !(*tbl).is_taken() {
                (*tbl).take();
                return tbl as *mut Table;
            }
        }

        // no free table met, we return null
        null_mut()
    }
}

struct AllocChunk {
    data: u32,
}

impl AllocChunk {

    const TAKEN_FLAG : u32 = 0x80_00_00_00;
    const SIZE_BITS  : u32 = 0x7f_ff_ff_ff;

    fn get_next(&mut self) -> *mut Self {
        unsafe { (self as *mut Self as *mut u8).add(self.get_size()) as *mut Self }
    }

    fn get_size(&self) -> usize {
        (self.data & Self::SIZE_BITS) as usize
    }

    fn is_taken(&self) -> bool {
        (self.data & Self::TAKEN_FLAG) != 0
    }

    fn take(&mut self) {
        self.data |= Self::TAKEN_FLAG
    }

    fn free(&mut self) {
        self.data &= !Self::TAKEN_FLAG
    }

    fn set_size(&mut self, size: usize) {
        self.data |= (size as u32) & Self::SIZE_BITS
    }

}

pub fn init() -> usize {
    unsafe {
        let origin : usize = 0x8000_0000 / (0x1000 * 0x400);
        
        // init page tables
        let pt_head = phys::alloc_pages(TOTAL_PAGE_TABLES).to_64_address() as *mut Table;
        assert!(pt_head != null_mut());

        for i in 0..TOTAL_PAGE_TABLES {
            (*pt_head.add(i)).free_anyway();
        }

        let zero_page = phys::alloc_pages(1);

        // init kernel heap

        let last_heap_page = usize::from(zero_page + NB_KERNEL_HEAP_PAGES);
        let last_heap_page_aligned = phys::align_ceil(last_heap_page, 10);
        let nb_kernel_heap_pages = last_heap_page_aligned - usize::from(zero_page) - 1;

        let kmalloc_head = phys::alloc_pages(nb_kernel_heap_pages).to_64_address() as *mut AllocChunk;
        assert!(kmalloc_head != null_mut());

        let first_free_page = phys::alloc_pages(1);
        let first_free_mega_page = usize::from(first_free_page) / 1024;

        assert_eq!(usize::from(first_free_page) & 0x3ff, 0);

        phys::free_pages(first_free_page);

        (*kmalloc_head).data = 0;

        GA = KernelAllocator {
            zero_page,
            kmalloc_head,
            pt_head,
        };

        phys::print_page_allocation();

        first_free_mega_page - origin
    }
}

#[alloc_error_handler]
fn alloc_error(layout: Layout) -> ! {
    println!("Couldnt allocate {} bytes", layout.size());
    loop { crate::cpu::sleep() }
}

pub fn reloc() {
//    unsafe {
//        GA.kmalloc_head = (GA.kmalloc_head as usize & !0x80000000) as *mut AllocChunk;
//        GA.pt_head = (GA.pt_head as usize & !0x80000000) as *mut Table;
//    }
}

pub fn print_kmem() {
    println!("[ALLOC] THE LIST");
    unsafe { GA.print_list() }
}
