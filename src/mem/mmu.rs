use core::{mem::size_of, marker::PhantomData, ops::{Add, Sub}, sync::atomic::{AtomicUsize, Ordering}};
use super::{alloc, PAGE_SIZE};
use crate::{
    lock::Mutex,
    cpu,
};

const PTE_SIZE : usize = size_of::<PTE>();
const NB_TABLE_ENTRIES : usize = PAGE_SIZE / PTE_SIZE;

/// This is the address mask used to translate every physical kernel address to virtual.
/// As the RAM is physically addressed at 0x8000_0000 and the kernel virtually addressed at
/// 0x0000_0000, to translate a physical kernel address to a virtual address, we mask out its MSB
pub static RELOC_MASK : Mutex<usize> = Mutex::new(0xffffffff);

#[derive(Clone, Copy, Debug)]
pub struct PTE(pub usize);

impl PTE {
    pub const VALID    : usize = 1;
    pub const READ     : usize = 2;
    pub const WRITE    : usize = 4;
    pub const EXECUTE  : usize = 8;
    pub const USER     : usize = 16;
    pub const GLOBAL   : usize = 32;
    pub const ACCESSED : usize = 64;
    pub const DIRTY    : usize = 128;
    pub const ALL      : usize = 0xff;

    pub const ACTIVE   : usize = 0x100;
    pub const PERMANENT: usize = 0x300;


    pub fn flags(&self) -> usize {
        self.0 & 0x0000_03ff
    }

    pub fn page_id(&self) -> PageId<Phys> {
        PageId::from_id(self.0 >> 10)
    }

    pub fn is_leaf(&self) -> bool {
        (self.0 & 0x0000_000e) != 0
    }

    pub fn is_valid(&self) -> bool {
        (self.0 & 1) != 0
    }

    pub fn set_flags(&mut self, flags:usize) {
        self.0 |= flags & 0x3ff
    }

    pub fn unset_flags(&mut self, flags:usize) {
        self.0 &= !(flags & 0x3ff)
    }

    pub fn ppn(&self) -> usize {
        self.0 >> 10
    }
}

#[derive(Clone)]
pub struct Table {
    entries: [PTE; NB_TABLE_ENTRIES],
}

impl Table {

    /// Maps a virtual page to a physical page.
    ///
    /// In the Sv32 page translation algorithm, the page table tree has 2 levels. If the leaf is
    /// at the first level, it means it addresses a contiguous memory chunk of size 1KiB * PAGE_SIZE
    /// (aligned to its size).
    ///
    /// The 2nd level PTE map at the page granularity.
    ///
    /// The function takes a `level` parameter to know which level of the tree will be a leaf and
    /// thuss which size is the memory chunk mapped.
    pub fn map(&mut self, vp: PageId<Virt>, pp: PageId<Phys>, level: usize, mask: usize) {
        let MASK = RELOC_MASK.spin_lock();
        // This is initialized to be the root
        let mut table = self as *mut Table;
        let mut pte = unsafe { &mut (*table).entries[vp.vpn(1)] };

        // This loop walks through the page table tree to get to the `level` we want.
        // We can allocate pages to build the table "on the fly" when reaching an invalid non-leaf
        // PTE
        for i in (level..1).rev() {
            if !pte.is_valid() {
                // a super-PTE is represented as a VALID entry with READ/WRITE/EXEC all 0
                //
                // ACCESSED and DIRTY bits are always on in our implementation because we don't
                // have page swapping yet
                let new_pt = alloc::alloc_page_table() as usize;
                let new_page = PageId::<Phys>::from_address(new_pt as usize | !*MASK);
                assert!(new_page.id != 0);
                *pte = new_page.to_pte(PTE::VALID | PTE::ACCESSED | PTE::DIRTY | (mask&!0xe));

                if mask & PTE::GLOBAL != 0 {
                    cpu::fence_global();
                } else {
                    cpu::fence_address(vp.to_64_address() as usize);
                }
            }

            table = ((pte.page_id().to_64_address() as usize) & *MASK) as *mut Table;
            pte = unsafe { &mut (*table).entries[vp.vpn(i)] };
        }

        // Here, the loop took care of setting the parents of the final PTE, and the variable
        // `pte` is normaly pointing to the good entry to edit
        *pte = pp.to_pte(
            PTE::VALID |
            PTE::ACCESSED |
            PTE::DIRTY |
            mask
        );

        if mask & PTE::GLOBAL != 0 {
           cpu::fence_global();
        } else {
            cpu::fence_address(vp.to_64_address() as usize);
        }
    }

    pub fn unmap(&mut self, vp: PageId<Virt>) {
        let mut table = self;
        for i in (0..=1).rev() {
            let pte = &mut table.entries[vp.vpn(i)];
            if pte.is_leaf() {
                //if (pte.flags() & PTE::PERMANENT) != PTE::PERMANENT {
                    pte.unset_flags(PTE::ALL);
                //}
                break
            } else {
                table = unsafe { ((pte.ppn() * PAGE_SIZE) as *mut Table).as_mut().unwrap() };
            }
        }
    }

    pub fn virt_to_phys(&self, va: usize) -> Option<u64> {
        let MASK = RELOC_MASK.spin_lock();
        let vp : PageId<Virt> = PageId::from_address(va);
    
        let pte1 = self.entries[vp.vpn(1)];
        if !pte1.is_valid() { return None }
        if pte1.is_leaf() {
            if pte1.ppn() & 0x3ff != 0 {
                return None
            }

            let mut ret = (pte1.ppn() as u64) << 12;
            ret |= (va & 0x003f_ffff) as u64;
            return Some(ret)
        }

        let subtable = unsafe { (((pte1.ppn() * PAGE_SIZE) & *MASK ) as *mut Table).as_mut().unwrap() };
        let pte0 = subtable.entries[vp.vpn(0)];
        
        if pte0.is_valid() {
            let mut ret = (pte0.ppn() as u64) << 12;
            ret |= (va & 0xfff) as u64;
            Some(ret)
        } else {
            None
        }
    }

    pub fn virt_to_pte(&self, va: usize) -> PTE {
        let MASK = RELOC_MASK.spin_lock();
        let vp : PageId<Virt> = PageId::from_address(va);
    
        let pte1 = self.entries[vp.vpn(1)];
        if pte1.is_leaf() || !pte1.is_valid() {
            return pte1;
        }

        let subtable = unsafe { (((pte1.ppn() * PAGE_SIZE) & *MASK ) as *mut Table).as_mut().unwrap() };
        let pte0 = subtable.entries[vp.vpn(0)];
 
        pte0
    }

    /// As pages are meant to be allocated from the page table allocator, we need a way to know if
    /// they are taken or not. The way we do it is by checking the unused bits of the first PTE of
    /// a Table. These bits are not used by the RISCV architecture, and can be freely used
    /// by the kernel as flags.
    ///
    /// If these flags are set, the page is taken. If they are unset, the page is not taken.
    pub fn is_taken(&self) -> bool {
        (self.entries[0].flags() & PTE::ACTIVE) != 0
    }

    pub fn is_permanent(&self) -> bool {
        (self.entries[0].flags() & PTE::PERMANENT) == PTE::PERMANENT
    }

    pub fn take(&mut self) {
        // use set_flags to prevent double-take to erase first PTE value
        self.entries[0].set_flags(PTE::ACTIVE);
    }

    pub fn take_permanent(&mut self) {
        // use set_flags to prevent double-take to erase first PTE value
        self.entries[0].set_flags(PTE::ACTIVE | PTE::PERMANENT);
    }

    pub fn free(&mut self) -> bool {
        if self.is_permanent() {
            false
        } else {
            self.entries[0].unset_flags(PTE::ACTIVE);
            true
        }
    }

    pub fn free_anyway(&mut self) {
        self.entries[0].unset_flags(PTE::PERMANENT);
    }

    pub fn entry(&self, i:usize) -> &PTE {
        &self.entries[i]
    }
}

impl Drop for Table {
    fn drop(&mut self) {
        let MASK = RELOC_MASK.spin_lock();
        for pte in &mut self.entries {
            if pte.is_valid() && !pte.is_leaf() {
                let addr = (pte.page_id().to_64_address() as usize & *MASK) as *mut Table;
                unsafe { (*addr).free(); }
            }
        }
        self.free();
    }
}

#[derive(Clone, Copy)]
pub struct Phys;
#[derive(Clone, Copy)]
pub struct Virt;

#[derive(Clone, Copy)]
pub struct PageId<T> {
    id: usize,
    phantom: PhantomData<T>,
}

impl<T> PageId<T> {
    pub const  fn from_address(addr: usize) -> Self {
        Self::from_id(addr >> 12)
    }

    pub const fn from_id(id: usize) -> Self {
        Self { id, phantom: PhantomData }
    }

    // A physical address in RV32 is on 34bits, and in the address translation algorithm
    // the 22 last bits stored in a PTE represent the 22 last bits of the resulting
    // physical address.
    //
    // The last 10 bits of a PTE are flags which tell the processor what kind of
    // page it points to
    //
    // In order to store the "real" address on a 32bits, we need to shift them
    // right twice again, for a total of 12
    //
    // We then store the result in a `u64` in order not to lose any information. The user can then
    // truncate these bits by casting to a `usize` to have an addressable number
    pub fn to_64_address(self) -> u64 {
        (self.id as u64) << 12
    }

    pub fn next(self) -> Self {
        Self::from_id(self.id.wrapping_add(1))
    }

    pub fn coerce<U>(self) -> PageId<U> {
        PageId::<U>::from_id(self.id)
    }

    pub fn null() -> Self {
        Self::from_id(0)
    }
    pub fn is_null(self) -> bool {
        self.id == 0
    }
}

impl<T> From<PageId<T>> for usize {
    fn from(p: PageId<T>) -> usize {
        p.id
    }
}

impl<T> Add<usize> for PageId<T> {
    type Output = Self;

    fn add(self, value: usize) -> Self {
        Self::from_id(self.id.wrapping_add(value))
    }
}

impl<T> Sub<usize> for PageId<T> {
    type Output = Self;

    fn sub(self, value: usize) -> Self {
        Self::from_id(self.id.wrapping_sub(value))
    }
}

impl PageId<Phys> {
    pub fn to_pte(self, flags: usize) -> PTE {
        PTE((self.id << 10) | flags)
    }

    pub fn ppn(self, level: usize) -> usize {
        match level {
            0 => (self.id) & 0x3ff,
            1 => (self.id >> 10) & 0xfff,
            _ => unreachable!(),
        }
    }
}

impl PageId<Virt> {
    pub fn vpn(self, level: usize) -> usize {
        match level {
            0 => self.id & 0x3ff,
            1 => (self.id >> 10) & 0x3ff,
            _ => unreachable!(),
        }
    }
}

/// Relocation of the MMU module resets the MSB of its mask. By doing so, all functions using this
/// mask will eventually mask out physical addresses MSB in the address translation process.
pub fn reloc() {
    *RELOC_MASK.spin_lock() = 0xffffffff;
}
