use crate::{
    cpu,
    mem::{
        PAGE_SIZE,
        alloc::alloc_page_table,
        virt::{KERNEL_VIRTUAL_SPACE, FIRST_USER_PAGE},
        mmu::{Table, PageId, PTE},
        phys::{alloc_pages, align_floor, align_ceil},
    },
    lock::Mutex,
    scheduler::Thread,
};
use alloc::{
    boxed::Box,
    sync::Arc,
};
use core::{
    ptr::NonNull,
    sync::atomic::{AtomicUsize, Ordering},
};
use plain::{Plain, Error as PlainError};
use goblin::{
    elf32::{
        header::*,
        program_header::*,
        section_header::*,
        sym::*,
    },
    strtab::*,
};

pub const STACK_SIZE: usize = PAGE_SIZE;

#[repr(C)]
pub struct Frame {
    pub pc: usize,
    pub iregs: [usize; 31],
}

impl Frame {
    pub fn print(&self) {
        println!("pc   : {:08x}", self.pc);
        println!("iregs: {:x?}", self.iregs);
    }
}

#[repr(C)]
pub struct Process {
    pub entry: *const (),
    pub gp: usize,
    pub nb_program_pages: usize,
    pub nb_heap_pages: usize,
    pub pid: usize,
    pub satp: usize,
}

/// SAFETY: Process can be Send because its only pointer member is its entry point, and it is always
/// a pointer to a Execute-only page.
///
/// However they cannot be Sync because they don't contain any mechanism for concurrent access to
/// the same instance.
unsafe impl Send for Process {}

impl Process {
    /// Create a process in the kernel heap. By doing so, we ensure no-one can build a Process from
    /// stack memory.
    pub fn load_elf(elf: &[u8]) -> Result<Box<Self>, PlainError> {
        let hd : &Header = Plain::from_bytes(&elf[..])?;
        let phs = ProgramHeader::slice_from_bytes(&elf[hd.e_phoff as usize..])?;
        let shs = SectionHeader::slice_from_bytes(&elf[hd.e_shoff as usize..])?;

        let strtable_desc = shs[hd.e_shstrndx as usize];
        let strtable = Strtab::from_slice_unparsed(&elf[..], strtable_desc.sh_offset as usize, strtable_desc.sh_size as usize, 0);
        
        let mut gp = 0;
        for sh in shs[..hd.e_shnum as usize].iter() {
            let name = &strtable[sh.sh_name as usize];
            if name == ".sdata" {
                gp = sh.sh_addr;
            }
        }

        gp += 2048;

        let mut first_byte = 0xffffffffusize;
        let mut last_byte = 0usize;

        for ph in phs[..hd.e_phnum as usize].iter() {
            if ph.p_type == 1 {
                first_byte = first_byte.min(ph.p_vaddr as usize);
                last_byte  = last_byte.max(ph.p_vaddr as usize + ph.p_memsz as usize);
            }
        }

        let entry = hd.e_entry as usize;
        let size = last_byte;
        let nb_pages = (size + PAGE_SIZE - 1) / PAGE_SIZE;

        let pid = alloc_pages(nb_pages);

        // copy address space
        let address_space = unsafe {
            let table = alloc_page_table() as *mut Table;
            let original_address_space = KERNEL_VIRTUAL_SPACE as usize as *mut Table;
            core::ptr::copy(original_address_space, table, 1);
            table
        };
        let satp = cpu::build_satp(address_space as usize, cpu::SatpMode::Sv32);
        let kernel_satp = cpu::get_satp();
        cpu::set_satp(satp);

        // create virtual mapping
        for i in 0..nb_pages {
            unsafe { (*address_space).map(PageId::from_id(i), pid + i, 0, PTE::USER | PTE::READ | PTE::WRITE | PTE::EXECUTE); }
        }

        let process_memory = unsafe { core::slice::from_raw_parts_mut(align_floor(0, 12) as *mut u8, PAGE_SIZE*(nb_pages)) };
        for ph in phs[..hd.e_phnum as usize].iter() {
            if ph.p_type == 1 {
                let fst_mem = ph.p_vaddr as usize;
                let size_mem = ph.p_memsz as usize;
                let fst_file = ph.p_offset as usize;
                let size_file = ph.p_filesz as usize;

                let size = size_mem.min(size_file);
                let last_mem = fst_mem + size;
                let last_file = fst_file + size;
                

                process_memory[fst_mem..last_mem].copy_from_slice(&elf[fst_file..last_file]);
            }
        }

        cpu::set_satp(kernel_satp);
        Ok(Box::new(Process {
            gp: gp as usize,
            entry: entry as *const (),
            nb_program_pages: nb_pages,
            nb_heap_pages: 0,
            pid: next_pid(),
            satp: satp as usize,
        }))
    }

    /// Create a user stack at the bottom of the virtual address space (0xffffbfff-0xffffffff) and maps
    /// it to allocated physical pages. We need a contiguous 
    pub fn make_stack(&mut self) -> usize {
        let mut stack_bottom = 0x8000_0000;
        loop {
            let phys = self.address_space().virt_to_phys(stack_bottom - 1);
            if phys.is_none() {
                break
            }

            stack_bottom -= STACK_SIZE;
        }

        let stack_top = stack_bottom - STACK_SIZE + 1;
        let nb_pages = STACK_SIZE / PAGE_SIZE; // WARNING: STACK_SIZE MUST BE A MULTIPLE OF PAGE_SIZE
        let phys = alloc_pages(nb_pages);

        for i in 0..nb_pages {
            let virt = PageId::from_address(stack_top + i * PAGE_SIZE);
            let phys = phys + i;
            self.address_space().map(virt, phys, 0, PTE::USER | PTE::READ | PTE::WRITE);
        }

        stack_bottom
    }

    /// Consumes the Box and moves the Process structure inside a (still heap allocated) Mutex. The
    /// Mutex is put inside an Arc and shared accross all Threads of the Process.
    ///
    /// As Threads are managed by the scheduler, we get a safe raw pointer to the thread (TODO:
    /// change it so we cannot deref or drop the pointed thread).
    ///
    /// Doing so gives us control over the lifetime of a Process: When no more Thread share the
    /// Process it is automatically droped because of the nature of Arc pointers.
    pub fn start(self: Box<Self>) -> Box<Thread> {
        let process = Arc::new(Mutex::new(*self));
        let stack_bottom;
        let mut frame;
        {
            let mut this = process.spin_lock();
            stack_bottom = this.make_stack();
            frame = Frame {
                iregs: [0; 31],
                pc: this.entry as usize,
            };
            frame.iregs[1] = stack_bottom;
            frame.iregs[7] = stack_bottom;
            frame.iregs[2] = this.gp;
        }
        let thread = Thread::new(frame, process, stack_bottom);

        thread
    }

    pub fn allocate_heap_pages(&mut self, size: usize) -> usize {
        let nb_pages = (size + PAGE_SIZE - 1) / PAGE_SIZE;
        let phys = alloc_pages(nb_pages);

        for i in 0..nb_pages {
            let virt = PageId::from_id(self.nb_program_pages + self.nb_heap_pages + i);
            let phys = phys + i;

            self.address_space().map(virt, phys, 0, PTE::USER | PTE::READ | PTE::WRITE)
        }

        let ret = STACK_SIZE * (self.nb_program_pages + self.nb_heap_pages);
        self.nb_heap_pages += nb_pages;
        ret
    }

    #[inline]
    pub fn address_space(&self) -> &mut Table {
        unsafe { ((self.satp << 12) as *mut Table).as_mut().unwrap() }
    }


    #[inline]
    pub fn satp(&self) -> usize {
        self.satp
    }
}

impl Drop for Process {
    fn drop(&mut self) {
        unsafe {
            (self.address_space() as *mut Table).drop_in_place();
        }
    }
}

static NEXT_PID : AtomicUsize = AtomicUsize::new(0);
fn next_pid() -> usize {
    NEXT_PID.fetch_add(1, Ordering::AcqRel)
}
