use core::ptr::NonNull;

#[repr(transparent)]
pub struct Desc<T>(NonNull<T>);

// For some reason, derive does not work for Desc, so I implemented by hand all needed traits

impl<T> Ord for Desc<T> {
    fn cmp(&self, rhs: &Self) -> core::cmp::Ordering {
        self.0.cmp(&rhs.0)
    }
}

impl<T> PartialOrd<Desc<T>> for Desc<T> {
    fn partial_cmp(&self, rhs: &Self) -> Option<core::cmp::Ordering> {
        self.0.partial_cmp(&rhs.0)
    }
}

impl<T> Clone for Desc<T> {
    fn clone(&self) -> Self {
        Self(self.0.clone())
    }
}
impl<T> Copy for Desc<T> {}

impl<T> core::cmp::PartialEq<Desc<T>> for Desc<T> {
    fn eq(&self, other: &Self) -> bool {
        self.0.eq(&other.0)
    }
}

impl<T> core::cmp::Eq for Desc<T> {}

// SAFETY: this is totaly safe since Desc does not give access to its internal state and its only
// interface is creation with `of` and comparison with Eq/Ord traits
unsafe impl<T> Send for Desc<T> {}
unsafe impl<T> Sync for Desc<T> {}

impl<T> Desc<T> {
    pub fn of(obj: &T) -> Self {
        Self(NonNull::from(obj))
    }
}
