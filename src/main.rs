#![no_std]
#![no_main]
#![feature(asm, panic_info_message, core_intrinsics, alloc_error_handler, naked_functions, cell_update, const_fn_trait_bound)]

extern crate alloc;
use alloc::{vec::Vec, boxed::Box};
use crate::fat32::BlkVolume;
use ::fat32 as libfat32;

#[macro_use]
/// UART0 primitives used to communicate with the outside world
pub mod uart;
/// RV32 Instructions parsing
pub mod isa;
/// PLIC management
pub mod plic;
/// Low-level RV32 core primitives (intrinsics)
pub mod cpu;
/// Constants of the target platform (addresses of MMIO and other stuff)
pub mod platform;
/// OS memory management
pub mod mem;
/// Trap entry point
pub mod trap;
/// Synchronization structures (Mutex, ...)
pub mod lock;
/// User process structure
pub mod process;
/// VirtIO driver
pub mod virtio;
/// Scheduler entry point and Thread management
pub mod scheduler;
/// Utility functions for managing elf files
pub mod elf;
/// Very basic fat32 volume driver for busy-waiting kernel-mode block driver
pub mod fat32;
/// A free list used to efficiently get and free IDs
pub mod free_list;
/// The mighty global object list, used by syscalls (see trap::handle_syscall)
pub mod object;
/// User-mode synchronization structures
pub mod sync;
/// Utility mod based on the core::lazy module. Re-implemented myself to allow some things the base
/// structure didn't allow
pub mod lazy;

pub mod descriptor;

use core::panic::PanicInfo;
use crate::uart::Uart;

extern "C" {
    fn goto_u(thread: Box<scheduler::Thread>, satp: usize);
}

#[no_mangle]
#[panic_handler]
extern "C" fn panic(info: &PanicInfo) -> ! {
    println!("Kernel Panic!");
    if let Some(p) = info.location() {
        println!("\t{}@{}: {}", p.file(), p.line(), info.message().unwrap());
    } else {
        println!("\tNo information available");
    }
    abort();
}

/// This function runs in machine mode (hence the m_ prefix) and is the last thing to run before
/// switching to Supervisor mode. It returns the value of the `satp` CSR which will be used by the
/// ASM part to setup virtual memory.
#[no_mangle]
extern "C" fn m_init() -> usize {


    dbg!("- Booting on Hart {}", cpu::get_hartid());
    dbg!("\n");
    dbg!("- Init UART");
    Uart::init();

    cpu::print_misa();

    dbg!("- Init PLIC");
    plic::set_threshold(0);
    plic::enable(10);
    plic::set_priority(10, 1);

    dbg!("- Init physical memory");
    mem::phys::init();
    let nb_kernel_mega_pages = mem::alloc::init();

    dbg!("- Init virtual memory");
    let satp = mem::virt::init(nb_kernel_mega_pages) as usize;

    cpu::build_satp(satp as usize, cpu::SatpMode::Sv32) as usize
}

#[no_mangle]
extern "C" fn s_init() -> ! {

    println!("We are in Supervisor-mode. The kernel will now launch its first User-mode process");

    // Discover devices, in particular the block device. We need it to access the volume and launch
    // the "init" process
    virtio::discover_devices();

    // setup a fat32 volume with the block device we just found
    let volume = libfat32::volume::Volume::new(BlkVolume::new());
    let root = volume.root_dir();
    let mut content = Vec::with_capacity(512);

    // search for a "init" file in the root folder
    let file = root.open_file("init").unwrap();

    // load the file if it exists
    for (buf, size) in file.read_per_sector() {
        content.extend_from_slice(&buf[..size]);
    }

    // create a process from the loaded file and start it (create its main thread)
    let init_proc = process::Process::load_elf(&content).unwrap();
    let satp = init_proc.satp();
    let init_thread = init_proc.start();

    // enable cpu timer
    platform::set_timeout(cpu::FREQ);

    // enable all PLIC interrupts (even though we just need the UART and Block Device)
    for i in 7..=22 {
        plic::enable(i);
        plic::set_priority(i, 1);
    }

    // switch to used-mode and jump to the init thread's pc
    unsafe { goto_u(init_thread, satp) }

    unreachable!("This is normaly unreachable because goto_u executes sret to jump at the first user thread's pc")
}

#[no_mangle]
extern "C" fn abort() -> ! {
    loop {
        cpu::nop()
    }
}
