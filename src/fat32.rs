use block_device::BlockDevice;
use crate::virtio;

#[derive(Clone, Copy)]
pub struct BlkVolume {
}

impl BlkVolume {
    pub fn new() -> Self { Self {} }
}

impl BlockDevice for BlkVolume {
    type Error = ();
    fn read(&self, buf: &mut [u8], address: usize, number_of_blocks: usize) -> Result<Self::Error, ()> {
        let mut buffer = [0;512];
        let first_sector = (address / 512) as u32;
        for i in 0..number_of_blocks {
            let sector = i as u32 + first_sector;
            virtio::block::read_blocking(sector, &mut buffer);
            buf[i*512..(i+1)*512].copy_from_slice(&buffer[..]);
        }
        Ok(())
    }

    fn write(&self, buf: & [u8], address: usize, number_of_blocks: usize) -> Result<Self::Error, ()> {
        let mut buffer = [0;512];
        let first_sector = (address / 512) as u32;
        for i in 0..number_of_blocks {
            let sector = i as u32 + first_sector;
            buffer[..].copy_from_slice(&buf[i*512..(i+1)*512]);
            virtio::block::write_blocking(sector, &mut buffer);
        }
        Ok(())
    }
}
