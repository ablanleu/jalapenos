// This is the cpu frequency in Hz, used to implement timers
pub const FREQ : u32 = 10_000_000;

#[inline]
pub fn get_misa() -> u32 {
    let ret;
    unsafe {
        asm!("csrr {0}, misa", out(reg) ret);
    }
    ret
}

pub fn print_misa() {
    let misa = get_misa();
    println!("MXL   ZYXWVUTSRQPONMLKJIHGFEDCBA");
    for i in 0..32 {
        print!("{}", (misa >> (31 - i)) & 1);
    }
    println!()
}

#[inline]
pub fn get_hartid() -> u32 {
    let ret;
    unsafe {
        asm!("csrr {0}, mhartid", out(reg) ret);
    }
    ret
}

#[inline]
pub fn get_mtval() -> u32 {
    let ret;
    unsafe {
        asm!("csrr {0}, mtval", out(reg) ret);
    }
    ret
}

#[inline]
pub fn get_stval() -> u32 {
    let ret;
    unsafe {
        asm!("csrr {0}, stval", out(reg) ret);
    }
    ret
}

#[inline]
pub fn get_sepc() -> u32 {
    let ret;
    unsafe {
        asm!("csrr {0}, sepc", out(reg) ret);
    }
    ret
}

#[inline]
pub fn get_mepc() -> u32 {
    let ret;
    unsafe {
        asm!("csrr {0}, mepc", out(reg) ret);
    }
    ret
}

#[inline]
pub fn sleep() {
    unsafe {
        asm!("wfi")
    }
}

#[inline]
pub fn nop() {
    unsafe {
        asm!("nop")
    }
}

pub enum SatpMode {
    Bare = 0 << 31,
    Sv32 = 1 << 31,
}

#[inline]
pub fn build_satp(root: usize, mode: SatpMode) -> u32 {
    mode as u32 | (root as u32 >> 12)
}

#[inline]
pub fn set_satp(value: u32) {
    unsafe {
        asm!("csrw satp, {0}", in(reg) value);
    }
    fence_global_asid(((value & 0x7fffffff) >> 22) as usize);
}

#[inline]
pub fn get_satp() -> u32 {
    let ret;
    unsafe {
        asm!("csrr {0}, satp", out(reg) ret);
    }
    ret
}

#[inline]
pub fn fence_address(vaddr: usize) {
    unsafe {
        asm!("sfence.vma {0}, zero", in(reg) vaddr);
    }
}

#[inline]
pub fn fence_global_asid(asid: usize) {
    unsafe {
        asm!("sfence.vma zero, {0}", in(reg) asid);
    }
}

#[inline]
pub fn fence_global() {
    unsafe {
        asm!("sfence.vma zero, zero");
    }
}

#[inline]
pub fn get_sscratch() -> u32 {
    let ret;
    unsafe {
        asm!("csrr {0}, sscratch", out(reg) ret);
    }
    ret
}

#[inline]
pub fn set_sscratch(value: u32) {
    unsafe {
        asm!("csrw sscratch, {0}", in(reg) value);
    }
}

#[inline]
pub fn swap_sscratch(value: u32) -> u32 {
    let ret;
    unsafe {
        asm!("csrrw {0}, scratch, {1}", out(reg) ret, in(reg) value);
    }
    ret
}

#[inline]
pub fn clear_sstatus(value: u32) {
    unsafe {
        asm!("csrrc zero, sstatus, {0}", in(reg) value);
    }
}
