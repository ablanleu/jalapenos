#ifndef __SYS_H__
#define __SYS_H__
void Puts(const char*, int);
void* PageAlloc(int);
void BlkRead(const char*, int, int);
void BlkWrite(const char*, int, int);
int Fork(void(*)(void*), void*);
void Join(int);
int SemaCreate(int);
void SemaFree(int);
void SemaP(int);
void SemaV(int);
int LockCreate(void);
void LockFree(int);
void LockLock(int);
void LockUnlock(int);
#endif // __SYS_H__
