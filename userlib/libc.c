#include "libc.h"
#include <stdarg.h>

int _print_int(int n) {
  char buf[32];
  int i = 31;
  
  do {
    buf[i--] = '0' + (n % 10);
    n /= 10;
  } while (n > 0);

  Puts(buf+i, 31-i);
  return 31-i;
}

int t_printf(const char* fmt, ...) {
  va_list args;
  va_start(args, fmt);
  char buffer[128];
  int cur = 0;
  int total = 0;

  while (*fmt) {
    if (cur == 128) {
      Puts(buffer, cur);
      total += cur;
      cur = 0;
    }

    if (*fmt == '%') {
      // flush previously written buffer
      Puts(buffer, cur);
      total += cur;
      cur = 0;

      ++fmt;
      switch (*fmt) {
        case 'd': _print_int(va_arg(args, int)); break;
        case 0: break;
        default: buffer[cur++] = *fmt;
      }
    } else {
      buffer[cur++] = *fmt;
    }

    ++fmt;
  }

  if (cur) {
    Puts(buffer, cur);
    total += cur;
  }

  va_end(args);

  return total;
}
