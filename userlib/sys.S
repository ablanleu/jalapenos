.global Puts
Puts:
	li a7, 0
	ecall
	ret
.global PageAlloc
PageAlloc:
	li a7, 1
	ecall
	ret
.global BlkRead
BlkRead:
	li a7, 2
	ecall
	ret
.global BlkWrite
BlkWrite:
	li a7, 3
	ecall
	ret
.global Fork
Fork:
	li a7, 4
	ecall
	ret
.global Join
Join:
	li a7, 5
	ecall
	ret
.global SemaCreate
SemaCreate:
	li a7, 6
	ecall
	ret
.global SemaFree
SemaFree:
	li a7, 7
	ecall
	ret
.global SemaP
SemaP:
	li a7, 8
	ecall
	ret
.global SemaV
SemaV:
	li a7, 9
	ecall
	ret
.global LockCreate
LockCreate:
	li a7, 10
	ecall
	ret
.global LockFree
LockFree:
	li a7, 11
	ecall
	ret
.global LockLock
LockLock:
	li a7, 12
	ecall
	ret
.global LockUnlock
LockUnlock:
	li a7, 13
	ecall
	ret
