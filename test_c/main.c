#include <libc.h>
#define NULL ((void*)0)
int sema;

typedef struct list {
  int value;
  struct list* next;
} list;

typedef struct {
  int flags;
  int size;
} memory_chunk;

memory_chunk* next_chunk(memory_chunk* chunk) {
  return (memory_chunk*)(((char*)chunk) + sizeof(memory_chunk) + chunk->size);
}

memory_chunk *first_chunk = NULL;
int current_capacity = 0;

int my_strlen(const char* str) {
  int ret = 0;
  for (;str[ret]!=0;++ret);
  return ret;
}

void my_puts(const char* str) {
  Puts(str, my_strlen(str));
}

memory_chunk* alloc_first_chunk(int size);
void* my_malloc(int size) {

  if (!first_chunk) {
    first_chunk = alloc_first_chunk(size);
  } else {
    memory_chunk* current = first_chunk;
    while (1) {
      if (current->flags == 0 && current->size < size) {
        current->flags = 1;
        return ((void*)current) + sizeof(memory_chunk);
      }

      memory_chunk *next = next_chunk(current);
      if (next->size == 0) {
        int needed = size + sizeof(memory_chunk);
        int remaining = current_capacity - ((void*)next - (void*)current);

        if (remaining < size + sizeof(memory_chunk)) {
          void* ptr = (void*)PageAlloc(size);
          current_capacity = ptr - (void*)first_chunk;
        }

        next->flags = 1;
        next->size = size;

        return next;
      }

      current = next;
    }
  }
}

memory_chunk* alloc_first_chunk(int size) {
  void* ptr = (void*)PageAlloc(size);
  memory_chunk* chunk = (memory_chunk*)ptr;

  chunk->flags = 1;
  chunk->size = size;

  memory_chunk* next = next_chunk(chunk);
  next->size = 0;

  return chunk;
}

void print_int(int n) {
  char buffer[32];
  char *cursor = buffer + 31;
  buffer[31] = 0;


  do {
    cursor--;
    int d = n % 10;
    n /= 10;
    *cursor = '0' + d;
  } while (n > 0);

  my_puts(cursor);
}

void read_block(char* buf, int len) {
  BlkRead(buf, 0, len);
}

int my_fork(void (*func)(void*), void* param) {
  return Fork(func, param);
}

int my_join(int thread) {
  Join(thread);
}

void thread_func(void*) {

  SemaP(sema);

  my_puts("forked thread finish\n");
}

int main(void) {

  sema = SemaCreate(0);

  my_puts("hello from C\n");

  list* link1 = my_malloc(sizeof(list));
  list* link2 = my_malloc(sizeof(list));

  link1->value = 10;
  link1->next = link2;
  link2->value = 20;
  link2->next = NULL;

  list* link = link1;
  while (link) {
    print_int(link->value);
    my_puts("\n");
    link = link->next;
  }

  int thread = my_fork(thread_func, NULL);
  my_puts("forked thread has id: ");
  print_int(thread);
  my_puts("\n");

  char buf[512];
  for (int i = 0; i < 512; ++i) {
    buf[i] = 0;
  }

  t_printf("coucou, voici l'adresse du buffer: %d", (int)(int*)buf);
  my_puts("\n");
  read_block(buf, 512);

  for (int i = 0; i < 512; ++i) {
    print_int(buf[i]);
    my_puts("; ");
  }

  SemaV(sema);
  my_join(thread);

  my_puts("goodbye from C\n");
}
